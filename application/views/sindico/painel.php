<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @brief view painel sindico
 * @author Marcus Vinicius Brum da Costa <marcus@kondotek.com.br>
 * @date   30/08/2015
 */

switch ($tela):
    case 'gerenciar':
    ?>
    <div class="painel_btns">
        <div class="btns_painel">
            <div id="btn_criar_aviso" class="btn_laranja button_painel_btns">Criar Aviso</div>
        </div>
        <div class="btns_painel">
            <a href="ocorrencias"><div class="btn_laranja button_painel_btns btn_ocorrencias">Ocorrências</div></a>
        </div>
        <div class="btns_painel">
            <a href="mensagens"><div class="btn_laranja button_painel_btns btn_mensagens">Mensagens</div></a>
        </div>
        <div class="btns_painel">
            <div class="btn_laranja button_painel_btns">Criar Votação</div>
        </div>
        <div class="btns_painel">
            <div class="btn_laranja button_painel_btns">Criar Ata</div>
        </div>
        <div class="btns_painel">
            <a href="#"><div class="btn_laranja button_painel_btns">Boletos</div></a>
        </div>
        <div class="btns_painel">
            <a href="#"><div class="btn_laranja button_painel_btns">Fotos</div></a>
        </div>
        <div class="btns_painel">
            <a href="#"><div class="btn_laranja button_painel_btns">Documentos</div></a>
        </div>
        <div class="btns_painel">
            <a href="#"><div class="btn_laranja button_painel_btns">Contas</div></a>
        </div>
    </div>
    <div id="cadastrar_aviso" style="display: none">
        <div class="titulo_padrao_dialogs titulo_padrao_dialogs_cinza">
            Cadastro de Avisos
        </div>
        <input type="text" name="titulo" value="" class="input_titulo" placeholder="Titulo" autofocus="">
        <br><br>

        <textarea name="texto" cols="40" rows="10" class="input_texto" placeholder="Texto"></textarea>
    </div>
<?php
    break;
    default :
        echo '<div class="alert-box alert"><p>A tela solicitada não existe</p></div>';
    break;
endswitch;
