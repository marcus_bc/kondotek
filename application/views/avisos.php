<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @brief view avisos
 * @author Marcus Vinicius Brum da Costa <marcus@kondotek.com.br>
 * @date   30/08/2015
 */

switch ($tela):
    case 'cadastrar':
        echo '<div class="avisos">';
        echo breadcrumb();
        erros_validacao('alert');
        get_msg('msgok');
        echo form_open('avisos/cadastrar', array('class'=>'frm_cadastro'));
        echo form_fieldset('Cadastrar novo aviso');
        echo form_label('Titulo');
        echo form_input(array('name'=>'titulo', 'class'=>'input_titulo', 'placeholder'=> 'Titulo'), set_value('titulo'), 'autofocus');
        echo '<br><br>';
        echo form_label('Texto');
        echo form_textarea(array('name'=>'texto', 'class'=>'input_texto', 'placeholder'=> 'Texto'), set_value('texto'));
        echo '<br>';
        echo anchor('avisos', '<input type="button" value="Cancelar" class="button_cinza btn_cancelar">');
        echo form_submit(array('name'=>'cadastrar', 'class'=>'button btn_cadastrar'), 'Salvar');
        echo form_fieldset_close();
        echo form_close();
        echo '</div>';
        break;
    case 'editar':
        $id_aviso = $this->uri->segment(3);
        if ($id_aviso==NULL):
                set_msg('msgerro', 'Escolha um aviso para editar', 'erro');
                redirect('avisos');
        endif; ?>
        <div class="avisos">
            <?php
                echo breadcrumb();
                $query = $this->avisos->get_byid($id_aviso)->row();
                erros_validacao('alert');
                get_msg('msgok');
                echo form_open(current_url(), array('class'=>'frm_cadastro'));
                echo form_fieldset('Editar aviso');
                echo form_label('Titulo');
                echo form_input(array('name'=>'titulo', 'class'=>'input_titulo', 'placeholder'=> 'Titulo'), set_value('titulo', $query->titulo), 'autofocus');
                echo '<br><br>';
                echo form_label('Texto');
                echo form_textarea(array('name'=>'texto', 'class'=>'input_texto', 'placeholder'=> 'Texto'), set_value('texto', $query->texto));
                echo '<br>';
                echo anchor('avisos', '<input type="button" value="Cancelar" class="button_cinza btn_cancelar">');
                echo form_submit(array('name'=>'editar', 'class'=>'button btn_cadastrar'), 'Salvar');
                echo form_hidden('id_aviso', $id_aviso);
                echo form_fieldset_close();
                echo form_close();
            ?>
        </div>
        <?php
    break;
    case 'gerenciar':
    ?>
    <div class="avisos">
        <?php
        echo breadcrumb();
        get_msg('msgok');
        get_msg('msgerro');
        ?>
        <div class="tabela">
            <?php
                if ($this->session->userdata('user_nivel') === '1'){
                    echo '<input type="button" value="Cadastrar Aviso" class="cad_aviso  btn_azul">';
                }
            ?>
            <table id="tb_avisos" class="table_style">
                <thead>
                    <tr>
                        <th>Titulo</th>
                        <th>Texto</th>
                        <?php if ($this->session->userdata('user_nivel') === '1'){ ?>
                        <th class="text-center">Ações</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    <div id="cadastrar_aviso">
        <div class="titulo_padrao_dialogs titulo_padrao_dialogs_cinza">
            Cadastro de Avisos
        </div>
        <input type="text" name="titulo" value="" class="input_titulo" placeholder="Titulo">
        <br><br>
        <textarea name="texto" cols="40" rows="10" class="input_texto" placeholder="Texto"></textarea>
    </div>
<?php
    break;
    default :
        echo '<div class="alert-box alert"><p>A tela solicitada não existe</p></div>';
    break;
endswitch;
