<!DOCTYPE html>
<html lang="pt-BR">
    <head>
	<meta charset="UTF-8">
        <title><?php if(isset($titulo)): ?>{titulo} | <?php endif; ?>{titulo_padrao}</title>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico" />
        <meta name="description" content="">
        <meta property="og:type" content="website"/>
        <meta property="og:title" content="Kondotek - Sistema para condominio do futuro"/>
        <meta property="og:description" content=""/>
        <meta property="og:url" content="http://kondotek.com.br"/>
        <meta property="og:image" content="http://kondotek.com.br/images/logo.png"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {css_head}
        {js_head}
    </head>
    <body>
        <header class="header">
            <div class="header_container">
                {logo}
                {acesso}
            </div>
            {menu}
        </header>
        <section id="container">
            {info_votacoes}
            {conteudo}
        </section>
        {rodape}
        {footerinc}
    </body>
</html>
