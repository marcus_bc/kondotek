<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @brief view mensagens
 * @author Marcus Vinicius Brum da Costa <marcus@kondotek.com.br>
 * @date   06/10/2015
 */

switch ($tela):
    case 'enviar':
        echo '<div class="mensagens">';
        echo breadcrumb();
        erros_validacao('alert');
        get_msg('msgok');
        echo form_open('mensagens/cadastrar', array('class'=>'frm_cadastro'));
        echo form_fieldset('Enviar mensagem', array('class'=>'fieldset_centralizado'));
        echo form_label('Titulo');
        echo form_input(array('name'=>'titulo', 'class'=>'input_titulo', 'placeholder'=> 'Titulo'), set_value('titulo'), 'autofocus');
        echo '<br><br>';
        echo form_label('Texto');
        echo form_textarea(array('name'=>'texto', 'class'=>'input_texto', 'placeholder'=> 'Texto'), set_value('texto'));
        echo '<br>';
        echo anchor('mensagens', '<input type="button" value="Cancelar" class="btn_cancelar btn_espacos">');
        echo form_submit(array('name'=>'cadastrar', 'class'=>'btn_cadastrar btn_espacos btn_confirmar'), 'Enviar');
        echo form_fieldset_close();
        echo form_close();
        echo '</div>';
        break;
    case 'gerenciar':
    ?>
    <div class="mensagens">
        <?php
        echo breadcrumb();
        get_msg('msgok');
        get_msg('msgerro');
        ?>
        <div class="tabela">
            <?php
                echo anchor("mensagens/enviar", '<input type="button" value="Enviar Mensagem" class="btn_azul btn_espacos cad_mensagem">');
            ?>
            <table id="tb_mensagens" class="table_style">
                <thead>
                    <tr>
                        <th>Titulo</th>
                        <th>Texto</th>
                        <th class="text-center">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $query = $this->mensagens->get_all()->result();
                    foreach ($query as $linha):
                        echo '<tr>';
                        printf('<td>%s</td>', $linha->titulo);
                        printf('<td>%s</td>', $linha->texto);
                        printf('<td class="texto_centralizado">%s%s</td>', anchor("mensagens/editar/$linha->id", ' ', array('class'=>'btn_editar', 'title'=>'Editar')), anchor("mensagens/excluir/$linha->id", ' ', array('class'=>'btn_excluir', 'title'=>'Excluir')));
                        echo '</tr>';
                    endforeach;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
<?php
    break;
    default :
        echo '<div class="alert-box alert"><p>A tela solicitada não existe</p></div>';
    break;
endswitch;
