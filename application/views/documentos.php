<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @brief view documentos
 * @author Wilson <wilson@kondotek.com.br>
 * @date   10/04/2016
 */

switch ($tela):
    case 'cadastrar':
        echo '<div class="documentos">';
        echo breadcrumb();
        erros_validacao('alert');
        get_msg('msgok');
        echo form_open_multipart('documentos/cadastrar', array('class'=>'frm_cadastro'));
        echo form_fieldset('Adicionar Documento');
        echo form_label('Titulo');
        echo form_input(array('name'=>'titulo', 'class'=>'input_titulo', 'placeholder'=> 'Titulo'), set_value('titulo'), 'autofocus');
        echo '<br><br>';
        echo form_label('Arquivo');
        echo form_upload(array('name'=>'arquivo', 'class'=>'input_arquivo', 'placeholder'=> 'Arquivo'), set_value('arquivo'));
        echo '<br>';
        echo anchor('documentos', '<input type="button" value="Cancelar" class="button_cinza btn_cancelar">');
        echo form_submit(array('name'=>'cadastrar', 'class'=>'button btn_cadastrar'), 'Salvar');
        echo form_fieldset_close();
        echo form_close();
        echo '</div>';
        break;
    case 'editar':

    break;
    case 'gerenciar':
    ?>
    <div class='documentos'>
        <?php
        echo breadcrumb();
        get_msg('msgok');
        get_msg('msgerro');
        ?>
        <div class='tabela'>
            <?php
                if ($this->session->userdata('user_nivel') === '1'){
                    echo '<input type="button" value="Adicionar Documento" class="cad_documento btn_azul">';
                }
            ?>
            <table id="tb_documento" class="table_style">
                <thead>
                    <tr>
                        <th>Titulo</th>
                        <th>Arquivo</th>
                        <?php if ($this->session->userdata('user_nivel') === '1'){ ?>
                        <th class="text-center">Ações</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    <div id="cadastrar_documento">
        <div class="titulo_padrao_dialogs titulo_padrao_dialogs_cinza">
            Adicionar documento
        </div>
        <input type="text" name="titulo" value="" class="input_titulo" placeholder="Titulo">
        <br><br>
        <input type='file' name='arquivo' class='input_arquivo' placeholder='Documento' />
    </div>
<?php
    break;
    default :
        echo '<div class="alert-box alert"><p>A tela solicitada não existe</p></div>';
    break;
endswitch;
