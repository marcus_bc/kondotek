<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @brief view ocorrencias
 * @author Marcus Vinicius Brum da Costa <marcus@kondotek.com.br>
 * @date   04/10/2015
 */

switch ($tela):
    case 'cadastrar':
        echo '<div class="ocorrencias">';
        echo breadcrumb();
        erros_validacao('alert');
        get_msg('msgok');
        echo form_open('ocorrencias/cadastrar', array('class'=>'frm_cadastro'));
        echo form_fieldset('Cadastrar nova ocorrência');
        echo form_label('Texto');
        echo form_textarea(array('name'=>'texto', 'class'=>'input_texto', 'placeholder'=> 'Texto'), set_value('texto'));
        echo '<br>';
        echo anchor('ocorrencias', '<input type="button" value="Cancelar" class="button_cinza btn_cancelar">');
        echo form_submit(array('name'=>'cadastrar', 'class'=>'button btn_cadastrar'), 'Salvar');
        echo form_fieldset_close();
        echo form_close();
        echo '</div>';
        break;
    case 'editar':
        $id_ocorrencia = $this->uri->segment(3);
        if ($id_ocorrencia==NULL):
                set_msg('msgerro', 'Escolha uma ocorrência para editar', 'erro');
                redirect('avisos');
        endif; ?>
        <div class="ocorrencias">
            <?php
                echo breadcrumb();
                $query = $this->ocorrencias->get_byid($id_ocorrencia)->row();
                erros_validacao('alert');
                get_msg('msgok');
                echo form_open(current_url(), array('class'=>'frm_cadastro'));
                echo form_fieldset('Editar ocorrencia');
                echo form_label('Texto');
                echo form_textarea(array('name'=>'texto', 'class'=>'input_texto', 'placeholder'=> 'Texto'), set_value('texto', $query->texto));
                echo '<br>';
                echo anchor('ocorrencias', '<input type="button" value="Cancelar" class="button_cinza btn_cancelar">');
                echo form_submit(array('name'=>'editar', 'class'=>'button btn_cadastrar'), 'Salvar');
                echo form_hidden('id_ocorrencia', $id_ocorrencia);
                echo form_fieldset_close();
                echo form_close();
            ?>
        </div>
        <?php
    break;
    case 'gerenciar':
    ?>
    <div class="ocorrencias">
        <?php
        echo breadcrumb();
        get_msg('msgok');
        get_msg('msgerro');
        ?>
        <div class="tabela">
            <?php
                if ($this->session->userdata('user_nivel') === '1'){
                    echo '<input type="button" value="Cadastrar Ocorrência" class="btn_espacos btn_azul cad_ocorrencia">';
                }
            ?>
            <table id="tb_ocorrencias" class="table_style">
                <thead>
                    <tr>
                        <th>Texto</th>
                        <?php if ($this->session->userdata('user_nivel') === '1'){ ?>
                        <th class="text-center">Ações</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
    <div id="cadastrar_ocorrencia">
        <div class="titulo_padrao_dialogs titulo_padrao_dialogs_laranja">Descreva a Ocorrência</div>
        <textarea name="texto" cols="40" rows="10" class="input_texto" placeholder="Texto"></textarea>
    </div>
<?php
    break;
    default :
        echo '<div class="alert-box alert"><p>A tela solicitada não existe</p></div>';
    break;
endswitch;
