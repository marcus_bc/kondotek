<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @brief view votacao
 * @author Marcus Vinicius Brum da Costa <marcus@kondotek.com.br>
 * @date   22/09/2015
 */

switch ($tela):
    case 'cadastrar':
        echo '<div class="votacao">';
        echo breadcrumb();
        erros_validacao('alert');
        get_msg('msgok');
        echo form_open('votacao/cadastrar', array('class'=>'frm_cadastro'));
        echo form_fieldset('Cadastrar Votação', array('class'=>'fieldset_centralizado'));
        echo form_label('Titulo');
        echo form_input(array('name'=>'titulo', 'class'=>'input_titulo', 'placeholder'=> 'Titulo'), set_value('titulo'), 'autofocus');
        echo '<br>';
        echo anchor('votacao', '<input type="button" value="Cancelar" class="btn_espacos btn_cancelar">');
        echo form_submit(array('name'=>'cadastrar', 'class'=>'btn_confirmar btn_espacos btn_cadastrar'), 'Salvar');
        echo form_fieldset_close();
        echo form_close();
        echo '</div>';
        break;
    case 'editar':
        $id_votacao = $this->uri->segment(3);
        if ($id_votacao==NULL):
                set_msg('msgerro', 'Escolha uma votação para editar', 'erro');
                redirect('votacao');
        endif; ?>
        <div class="votacao">
            <?php
                echo breadcrumb();
                $query = $this->votacao->get_byid($id_votacao)->row();
                erros_validacao('alert');
                get_msg('msgok');
                echo form_open(current_url(), array('class'=>'frm_cadastro'));
                echo form_fieldset('Editar votação');
                echo form_label('Titulo');
                echo form_input(array('name'=>'titulo', 'class'=>'input_titulo', 'placeholder'=> 'Titulo'), set_value('titulo', $query->titulo), 'autofocus');
                echo '<br>';
                echo anchor('votacao', '<input type="button" value="Cancelar" class="button_cinza btn_cancelar">');
                echo form_submit(array('name'=>'editar', 'class'=>'button btn_cadastrar'), 'Salvar');
                echo form_hidden('id_votacao', $id_votacao);
                echo form_fieldset_close();
                echo form_close();
            ?>
        </div>
        <?php
    break;
    case 'gerenciar':
    ?>
    <div class="votacao">
        <?php
        echo breadcrumb();
        get_msg('msgok');
        get_msg('msgerro');
        ?>
        <div class="tabela">
            <?php
                if ($this->session->userdata('user_nivel') === '1'){
                    echo anchor("votacao/cadastrar", '<input type="button" value="Cadastrar Votação" class="btn_azul btn_espacos cad_votacao">');
                }
            ?>
            <table id="tb_votacao" class="table_style">
                <thead>
                    <tr>
                        <th>Titulo</th>
                        <?php if ($this->session->userdata('user_nivel') === '1'){ ?>
                        <th class="text-center">Ações</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $query = $this->votacao->get_all()->result();
                    foreach ($query as $linha):
                        echo '<tr>';
                        printf('<td>%s</td>', $linha->titulo);
                        if ($this->session->userdata('user_nivel') === '1'){
                            printf('<td class="texto_centralizado">%s%s</td>', anchor("votacao/editar/$linha->id", ' ', array('class'=>'btn_editar', 'title'=>'Editar')), anchor("votacao/excluir/$linha->id", ' ', array('class'=>'btn_excluir', 'title'=>'Excluir')));
                        }
                        echo '</tr>';
                    endforeach;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
<?php
    break;
    default :
        echo '<div class="alert-box alert"><p>A tela solicitada não existe</p></div>';
    break;
endswitch;
