<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @brief view login
 * @author Marcus Vinicius Brum da Costa <marcus@kondotek.com.br>
 * @date   30/08/2015
 */

switch ($tela):
    case 'login':
    ?>
    <div class="login">
        <?php get_msg('logoffok');
            get_msg('errologin');
            erros_validacao(); ?>
        <?php
            echo form_open('login', array('class'=>'loginform'));
            echo form_label('Email', 'email');
            echo form_input(array('id'=>'email', 'name'=>'email', 'class'=>'input_login_email', 'placeholder'=>'E-mail'), set_value('email'), 'autofocus');
            echo form_label('Senha', 'senha');
            echo form_password(array('id'=>'email', 'name'=>'senha', 'class'=>'input_login_senha', 'placeholder'=>'Senha'), set_value('senha'));
            echo form_hidden('redirect', $this->session->userdata('redir_para'));
            echo form_submit(array('name'=>'entrar', 'class'=>'btn_laranja button_login'), 'Entrar');
            echo form_close();
            echo '<span id="recovery_login">';
            echo form_checkbox('lembrar', 'lembrar', TRUE).'Lembrar-me';
            echo anchor('login/nova_senha', 'Esqueceu sua senha? Clique aqui.');
            echo '</span>';
        ?>
    </div>
<?php
    break;
    case 'nova_senha':
        echo '<div id="nav">';
        echo '<div id="logo">';
        echo '<img src="'.base_url().'uploads/img/1/logo.png" alt="logo">';
        echo '</div>';
        echo '</div>';
        echo '<div>';
        echo form_open('login/nova_senha', array('class'=>'frm_usuarios'));
        echo form_fieldset('', array("id" => "fieldset_recupera_senha"));
        erros_validacao();
        get_msg('msgok');
        get_msg('msgerro');
        echo form_label('Seu email');
        echo '<br />';
        echo '<table id="table_recupera_senha"><tr><td><strong class="label_texto">Digite seu E-mail:</strong></td></tr><tr><td>';
        echo form_input(array('name'=>'email', 'style'=>'width:300px;'), set_value('email'), 'autofocus');
        echo '</td></tr>';
        echo gera_captcha();

        echo '<tr><td><strong>Digite os caracteres que aparecem na imagem acima:</strong></td></tr>';
        echo '<tr><td><input type="text" name="captcha" value="" style="text-align: left; width: 300px" /></td></tr>';
        echo '<tr><td style="text-align:center">';
        echo form_submit(array('name'=>'nova_senha', 'class'=>'btn_confirmar btn_espacos'), 'Enviar nova senha');
        echo '</td>';
        echo '</tr></table>';
        echo '<br /><br />';
        echo anchor('login', 'Voltar', array('class'=>'button_red btn_voltar_login', 'style'=>'float:left; '));

        echo form_fieldset_close();
        echo form_close();
        echo '</div>';
    break;
    default :
       echo '<div class="alert-box alert" ><p>A tela solicitada não existe</p></div>';
    break;
endswitch;
