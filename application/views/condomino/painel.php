<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @brief view painel condomino
 * @author Marcus Vinicius Brum da Costa <marcus@kondotek.com.br>
 * @date   13/09/2015
 */

switch ($tela):
    case 'gerenciar':
    ?>
    <div class="ultimos_avisos">
        Últimos Avisos
    </div>
    <div class="board">
        <?php
        $avisos = $this->painel->get_all_avisos()->result();
        foreach ($avisos as $linha_avisos):
            echo ($linha_avisos->visualizado=='t') ? '<div class="avisos_visualizados">' : '<div class="avisos_n_visualizados">';
            printf('<div class="texto_avisos">%s<br><br>%s<br><br>%s%s</div>', 
                                              $linha_avisos->titulo, $linha_avisos->texto, $linha_avisos->data_cadastro,
                                              '<input type="hidden" class="id_aviso" value="'.$linha_avisos->id.'" />');
            echo '</div>';
        endforeach;
        ?>
    </div>
    <div class="carousel">
    
    </div>
    <div class="votacao">
        <div class="teste">
            <?php
            $votacao = $this->painel->get_all_votacao()->result();
            foreach ($votacao as $linha_votacao):
                echo '<div>';
                echo '<span class="titulo">Votação</span>';
                echo '<span class="texto_votacao">';
                printf('%s', $linha_votacao->titulo);
                echo '</span>';
                echo '<span class="btns_votacao">';
                echo '<button class="button btn_votacao">Sim</button>';
                echo '<button class="button btn_votacao">Não</button>';
                echo '</span>';
                echo '<span class="ata">';
                echo 'Clique aqui para ler a ata';
                echo '</span>';
                echo '</div>';
                
            endforeach;
            ?>
        </div>
    </div>
<?php
    break;
    default :
        echo '<div class="alert-box alert"><p>A tela solicitada não existe</p></div>';
    break;
endswitch;