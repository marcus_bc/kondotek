<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @brief view inicio
 * @author Marcus Vinicius Brum da Costa <marcus@kondotek.com.br>
 * @date   22/09/2015
 */

switch ($tela):
    case 'inicio':
    ?>
    <section class="introducao">
        <div class="introducao_container">
            <h1 class="titulo">Kondotek é um sistema para/de gestão de condominios e votação online.</h1>
            <h2>Funcionalidades</h2>
            <h3>
                <ul>
                    <li>Facilitar a comunicação entre sindico e condominos</li>
                    <li>Votação online de assembleias</li>
                    <li>Prestação de contas</li>
                    <li>Contas a pagar e a receber</li>
                    <li>Informações atualizadas sobre o condominio</li>
                    <li>Mural de avisos online</li>
                </ul>
            </h3>
            <input type="button" value="Saiba mais" class="button button_saiba_mais">
        </div>
    </section>
    <aside class="login_contato">
        <div class="login">
            <?php
                echo anchor("login", '<input type="button" value="Entrar" class="button btn_entrar">');
            ?>
        </div>
        <div class="contato">
            <br>
            <?php
                echo '<h3>Para orçamento, dúvidas ou contato, preencha o formulário abaixo:</h3>';
                echo form_open('contato', array('class'=>'contatoform'));
                echo form_input(array('name'=>'nome', 'class'=>'input_contato_nome', 'placeholder'=>'Nome'), set_value('nome'));
                echo '<br /><br />';
                echo form_input(array('name'=>'email', 'class'=>'input_contato_email', 'placeholder'=>'E-mail'), set_value('email'));
                echo '<br /><br />';
                echo form_input(array('name'=>'telefone', 'class'=>'input_contato_telefone', 'placeholder'=>'Telefone'), set_value('telefone'));
                echo '<br /><br />';
                echo form_input(array('name'=>'estado', 'class'=>'input_contato_estado', 'list'=>'estado', 'placeholder'=>'Estado'), set_value('estado'));
                echo '<datalist id="estado">';
                echo '<option value="Rio grande do Sul">';
                echo '<option value="Santa Catarina">';
                echo '<option value="Paraná">';
                echo '</datalist>';
                echo '<br /><br />';
                echo form_input(array('name'=>'cidade', 'class'=>'input_contato_cidade', 'placeholder'=>'Cidade'), set_value('cidade'));
                echo '<br /><br />';
                echo form_textarea(array('name'=>'mensagem', 'class'=>'input_contato_mensagem', 'placeholder'=>'Mensagem'), set_value('mensagem'));
                echo form_submit(array('name'=>'enviar', 'class'=>'button button_enviar'), 'Enviar');
                echo form_close();
            ?>
        </div> 
    </aside>      
<?php
    break;
    default :
        echo '<div class="alert-box alert"><p>A tela solicitada não existe</p></div>';
    break;
endswitch;