<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//carrega um módulo do sistema devolvendo a tela solicitada
function load_modulo($modulo=NULL, $tela=NULL, $dados=NULL){
    $CI =& get_instance();
    if ($modulo!=NULL){
        return $CI->load->view("$modulo", array('tela'=>$tela, 'dados'=>$dados), TRUE);
    }else{
        return FALSE;
    }
}

//seta valores ao array $tema da classe sistema
function set_tema($prop, $valor, $replace=TRUE){
    $CI =& get_instance();
    $CI->load->library('sistema');
    if ($replace){
        $CI->sistema->tema[$prop] = $valor;
    }else{
        if(!isset($CI->sistema->tema[$prop])){
            $CI->sistema->tema[$prop] = '';
        }
        $CI->sistema->tema[$prop] .= $valor;
    }
}

//retorna os valores do array $tema da classe sistema
function get_tema(){
    $CI =& get_instance();
    $CI->load->library('sistema');
    return $CI->sistema->tema;
}

//inicializa o sistema carregando os recursos necessários
function init_painel($tela = "default"){
    $CI =& get_instance();
    $CI->load->library(array('parser', 'sistema', 'session', 'form_validation'));
    $CI->load->helper(array('form', 'url', 'array', 'text'));

    //carregamento dos models
    //$CI->load->model('usuarios_model', 'usuarios');
    //$CI->load->model('login_model', 'login');

    //date_default_timezone_set('America/Sao_Paulo');

    set_tema('css_head', load_css(array('default')), FALSE);
    set_tema('css_head', load_css(array('normalize')), FALSE);
    set_tema('css_head', load_css(array('jquery-ui')), FALSE);
    set_tema('css_head', load_css(array('noty')), FALSE);
    set_tema('js_head', load_js(array('jquery')), FALSE);
    set_tema('js_head', load_js(array('jquery-ui')), FALSE);
    set_tema('footerinc', load_js(array('libs/jquery.noty.packaged.min')), FALSE);
    set_tema('footerinc', load_js(array('noty')), FALSE);

    set_tema('titulo_padrao', 'Kondotek');
    //set_tema('rodape', '<footer id="footer_rodape"><span>&copy; 2015 | Todos os direitos reservados para Kondotek</span></footer>');
    set_tema('logo', '');
    set_tema('acesso', '');
    set_tema('menu', '');
    set_tema('info_votacoes', '');
    set_tema('rodape', '');
    set_tema('template', $tela);
    //set_tema('menu', $CI->usuarios->gera_menu());
    //set_tema('breadcrumb', breadcrumb());
    //set_tema('headerinc', load_js(array('jquery-1.10.2.min')), FALSE);
    //set_tema('headerinc', load_js(array('jquery-ui')), FALSE);

}

//inicializa o tinymce para criação de textarea com editor html
function init_htmleditor(){
    //set_tema('footerinc', load_js(base_url('htmleditor/tiny_mce.js'), NULL, TRUE), FALSE);
    //set_tema('footerinc', load_js(base_url('htmleditor/init_tiny_mce.js'), NULL, TRUE), FALSE);

}

function init_datatables(){
    set_tema('css_head', load_css(array('jquery.dataTables.min')), FALSE);
    set_tema('footerinc', load_js(array('jquery.dataTables.min')), FALSE);
    set_tema('footerinc', load_js(array('defaults_datatable')), FALSE);
}

//retorna ou printa o conteudo de uma view
function incluir_arquivo($view, $pasta='includes', $echo=TRUE){
    $CI =& get_instance();
    if($echo == TRUE){
        echo $CI->load->view("$pasta/$view", '', TRUE);
        return TRUE;
    }
    return $CI->load->view("$pasta/$view", '', TRUE);
}

//carrega um template passando o array $tema como parâmetro
function load_template(){
    $CI =& get_instance();
    $CI->load->library('sistema');
    $CI->parser->parse($CI->sistema->tema['template'], get_tema());
}

//carrega um ou varios arquivos .css de uma pasta
function load_css($arquivo=NULL, $pasta='css', $media='all'){
    if($arquivo != NULL){
        $CI =& get_instance();
        $CI->load->helper('url');
        $retorno = '';
        if(is_array($arquivo)){
            foreach ($arquivo as $css){
                $retorno .= '<link rel="stylesheet" type="text/css" href="'.base_url("$pasta/$css.css").'" media="'.$media.'" />'."\n";
            }
        }else{
           $retorno = '<link rel="stylesheet" type="text/css" href="'.base_url("$pasta/$arquivo.css").'" media="'.$media.'" />'."\n";
        }
    }
    return $retorno;
}

//carrega um ou varios arquivos .js de uma pasta ou servidor remoto
function load_js($arquivo=NULL, $pasta='js', $remoto=NULL){
    if($arquivo != NULL){
        $CI =& get_instance();
        $CI->load->helper('url');
        $retorno = '';
        if(is_array($arquivo)){
            foreach ($arquivo as $js){
                if($remoto){
                    $retorno .= '<script type="text/javascript" src="'.$js.'"></script>';
                }else{
                    $retorno .= '<script type="text/javascript" src="'.base_url("$pasta/$js.js").'"></script>';
                }
            }
        }else{
           if($remoto){
                    $retorno .= '<script type="text/javascript" src="'.$arquivo.'"></script>';
           }else{
                    $retorno .= '<script type="text/javascript" src="'.base_url("$pasta/$arquivo.js").'"></script>';
           }
        }
    }
    return $retorno;
}

//Retorna o item ativo do menu
function activate_menu($controller) {
    $CI = get_instance();
    $class = $CI->router->fetch_class();
    return ($class == $controller) ? 'ativo' : '';
}

function gera_menu(){

    $menu = '<nav>
                <ul>
                    <li>
                        <a href="'.base_url("painel").'" class="'.activate_menu('painel').'">Inicio</a>
                    </li>
                    <li>
                        <a href="'.base_url("avisos").'" class="'.activate_menu('avisos').'">Avisos</a>
                    </li>
                    <li>
                        <a href="'.base_url("votacao").'" class="'.activate_menu('votacao').'">Votações</a>
                    </li>
                    <li>
                        <a href="'.base_url("ocorrencias").'" class="'.activate_menu('ocorrencias').'">Ocorrências</a>
                    </li>
                    <li>
                        <a href="'.base_url("mensagens").'" class="'.activate_menu('mensagens').'">Mensagens</a>
                    </li>
                    <li>
                        <a href="'.base_url("documentos").'" class="'.activate_menu('documentos').'">Documentos</a>
                    </li>
                    <li>
                        <a href="'.base_url("boletos").'" class="'.activate_menu('boletos').'">Boletos</a>
                    </li>
                    <li>
                        <a href="'.base_url("login/logoff").'">Sair</a>
                    </li>
                </ul>
            </nav>';
    return $menu;
}

function gera_acesso(){
    $CI =& get_instance();
    return '<div class="acesso">
                    <p>'.$CI->session->userdata('user_nome').'</p>
               </div>';
}

function gera_alertas(){
    $CI =& get_instance();
    if($CI->session->userdata('user_nivel') === '0'){
        $CI->load->model('votacao_model', 'votacao');
        $query = $CI->votacao->get_count_ativas()->row();
        $alertas = '<div class="info_votacoes"><p>'.$query->qtde.' Votação ativa</p></div>';
    }elseif($CI->session->userdata('user_nivel') === '1'){
        $alertas = '<br>';
    }
    return $alertas;
}

function carrega_logo($id_condominio=NULL){
    if($id_condominio !== NULL){
        $logo = '<a href="'.base_url().'">
                    <img src="'.base_url().'uploads/img/'.$id_condominio.'/logo.png" alt="Logo" />
                 </a>';
    }else{
        $logo = '<a href="'.base_url().'">
                    <img src="'.base_url().'images/logo.png" alt="Logo" />
                 </a>';
    }
    return $logo;
}

//Utiliza o tempo ocioso do browser para fazer o download ou buscar doumentos que talvez serão usados no futuro pelo usuário
function load_prefetch($arquivo=NULL, $pasta='images'){
    if($arquivo != NULL){
        $CI =& get_instance();
        $CI->load->helper('url');
        $retorno = '';
        if(is_array($arquivo)){
            foreach ($arquivo as $images){
                $retorno .= '<link rel="prefetch" href="'.base_url("$pasta/$images").'" />';
            }
        }else{
           $retorno = '<link rel="prefetch" href="'.base_url("$pasta/$arquivo").'" />';
        }
    }
    return $retorno;
}

//mostra erros de validação em forms
function erros_validacao($alert=NULL){
    if(validation_errors()){
        if($alert !== NULL){
            echo '<div class="alert-box alert">'.validation_errors('<p>', '</p>').'</div>';
        }else{
            echo '<div class="alert-box">'.validation_errors('<p>', '</p>').'</div>';
        }
    }
}

//verifica se o usuario está logado no sistema
function esta_logado($redir=TRUE){
    $CI =& get_instance();
    $CI->load->library('session');
    $user_status = $CI->session->userdata('user_logado');
    if(!isset($user_status) || $user_status != TRUE){
        if($redir){
            $CI->session->set_userdata(array('redir_para'=>current_url()));
            set_msg('errologin', 'Acesso restrito, faça login antes de prosseguir', 'erro');
            redirect('login');
        }else{
            return FALSE;
        }
    }else{
        return TRUE;
    }
}

//define uma mensagem para ser exibida na proxima tela carregada
function set_msg($id='msgerro', $msg=NULL, $tipo='erro'){
    $CI =& get_instance();
    switch ($tipo){
        case 'erro':
            $CI->session->set_flashdata($id, '<div class="alert-box alert"><p>'.$msg.'</p></div>');
            break;
        case 'sucesso':
            $CI->session->set_flashdata($id, '<div class="alert-box success"><p>'.$msg.'</p></div>');
            break;
        default:
            $CI->session->set_flashdata($id, '<div class="alert-box"><p>'.$msg.'</p></div>');
            break;
    }
}

//verifica se existe uma mensagem para ser exibida na tela atual
function get_msg($id, $printar=TRUE){
    $CI =& get_instance();
    if($CI->session->flashdata($id)){
        if($printar){
            echo $CI->session->flashdata($id);
            return TRUE;
        }else{
            return $CI->session->flashdata($id);
        }
    }
    return FALSE;
}

//verifica se o usuario atual é administrador
function is_admin($set_msg=FALSE){
    $CI =& get_instance();
    $user_admin = $CI->session->userdata('user_admin');
    if(!isset($user_admin) || $user_admin != TRUE){
        if($set_msg){
            set_msg('msgerro', 'Seu usuário não tem permissão para executar esta operação', 'erro');
            return FALSE;
        }
    }else{
        return TRUE;
    }
}

//gera um breadcrumb com base no controller atual

function breadcrumb(){
    $CI =& get_instance();
    $CI->load->helper('url');
    $classe = ucwords(str_replace('_', ' ', $CI->router->class));
    if($classe == 'Painel'){
        $classe = anchor($CI->router->class, 'Início');
        $inicio = '';
    }else{
        $classe = anchor($CI->router->class, $classe);
        $inicio = anchor('painel', 'Início').' &raquo;';
    }
    $metodo = ucwords(str_replace('_', ' ', $CI->router->method));
    if($metodo && $metodo != 'Index'){
        $metodo = " &raquo; ".anchor($CI->router->class."/".$CI->router->method, $metodo);
    }else{
        $metodo = '';
    }
    return '<p class="breadcrumb">Sua localização: '.$inicio.' '.$classe.$metodo.'</p>';

}

function retorna_pagina(){
    $CI =& get_instance();
    $CI->load->helper('url');
    $pagina = $CI->router->class;
    return ucfirst($pagina);
}

//seta um registro na tabela de auditoria
function auditoria($operacao, $obs='', $query=TRUE){
    $CI =& get_instance();
    $CI->load->library('session');
    $CI->load->model('auditoria_model', 'auditoria');
    if($query){
        $last_query = $CI->db->last_query();
    }else{
        $last_query = '';
    }
    if(esta_logado(FALSE)){
        $user_id = $CI->session->userdata('user_id');
        $empresa_id = $CI->session->userdata('user_id_empresa');
    }
    $dados = array(
        'data_hora' => date("Y-m-d H:i:s"),
        'operacao' => $operacao,
        'query' => $last_query,
        'observacao' => $obs,
        'id_usuario' => $user_id,
        'id_empresa' => $empresa_id,
    );
    $CI->auditoria->do_insert($dados);
}

function cria_diretorio($dir){
    if(is_dir($dir)){
            return true;
    }else{
        if(mkdir($dir, 0777, true)){
            //Se criou o diretorio, cria o index.html
            $pagename = $dir."/index.html";
            $texto = "<html><head><title>403 Forbidden</title></head>
                      <body><p>Directory access is forbidden.</p></body>
                      </html>";
            $fp = fopen($pagename , "w");
            $fw = fwrite($fp, $texto);
            //Verificar se o arquivo foi salvo.
            if($fw == strlen($texto)){
               return true;
            }else{
               return false;
            }
        }else{
            return false;
        }
    }
}

//gera uma miniatura de uma imagem caso ela ainda não exista
function thumb($imagem=NULL, $largura=100, $altura=75, $geratag=TRUE){
    $CI =& get_instance();
    $CI->load->helper('file');
    $thumb = $largura.'x'.$altura.'_'.$imagem;
    $thumbinfo = get_file_info('./uploads/thumbs/'.$thumb);
    if($thumbinfo != FALSE){
        $retorno = base_url('uploads/thumbs/'.$thumb);
    }else{
        $CI->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = './uploads/'.$imagem;
        $config['new_image'] = './uploads/thumbs/'.$thumb;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $largura;
        $config['height'] = $altura;
        $CI->image_lib->initialize($config);
        if($CI->image_lib->resize()){
            $CI->image_lib->clear();
            $retorno = base_url('uploads/thumbs/'.$thumb);
        }else{
            $retorno = FALSE;
        }
    }
    if($geratag && $retorno != FALSE){
        $retorno = '<img src="'.$retorno.'" alt="" />';
        return $retorno;
    }
}

/*
function moeda_to_db($get_valor=NULL){
    if($get_valor != NULL){
        $source = array('.', ',');
        $replace = array('', '.');
        $valor = str_replace($source, $replace, $get_valor);
        return $valor;
    }
}

*/

function data_to_db($get_data=NULL, $ini_fim=NULL){
    //Retorna a data sem hora
    if($get_data !== NULL && $ini_fim === NULL){
        $get_data = explode("/",$get_data);
        $data   = $get_data[2].'-'.$get_data[1].'-'.$get_data[0];
        return $data;
    //Retorna a data com hora 00:00:00
    }elseif($get_data !== NULL && $ini_fim === '1'){
        $get_data = explode("/",$get_data);
        $data   = $get_data[2].'-'.$get_data[1].'-'.$get_data[0].' 00:00:00';
        return $data;
    //Retorna a data com hora 23:59:59
    }elseif($get_data !== NULL && $ini_fim === '2'){
        $get_data = explode("/",$get_data);
        $data   = $get_data[2].'-'.$get_data[1].'-'.$get_data[0].' 23:59:59';
        return $data;
    }
}

function gera_captcha(){
    $CI =& get_instance();
    $CI->load->helper('captcha');
    $vals = array(
        'img_path'	=> './captcha/',
        'img_url'	=> 'http://localhost/captcha/',
        'img_width'	=> '150',
        'img_height'    => 30,
        'expiration'    => 7200
    );
    $cap = create_captcha($vals);
    return $cap['image'];
}
