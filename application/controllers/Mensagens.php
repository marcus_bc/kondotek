<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mensagens extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_painel();
        $this->load->model('mensagens_model', 'mensagens');
        init_datatables();
        set_tema('css_head', load_css(array('mensagens')), FALSE);
        set_tema('footerinc', load_js(array('funcoes_mensagens')), FALSE);
        set_tema('logo', carrega_logo($this->session->userdata('user_id_condominio')));
        set_tema('acesso', gera_acesso());
        set_tema('menu', gera_menu());
        set_tema('info_votacoes', gera_alertas());
    }

    public function index() {
        $this->gerenciar();
    }

    public function gerenciar() {
        esta_logado();
        set_tema('titulo', 'Mensagens');
        set_tema('conteudo', load_modulo('mensagens', 'gerenciar'));
        load_template();
    }

    public function enviar(){
        $this->form_validation->set_rules('titulo', 'Titulo', 'trim|required|ucwords');
        if ($this->form_validation->run()==TRUE):
            $dados['titulo']        = mb_strtoupper($this->input->post('titulo'), 'UTF-8');
            $dados['texto']         = mb_strtoupper($this->input->post('texto'), 'UTF-8');
            $dados['id_usuario']    = $this->session->userdata('user_id');
            $dados['id_condominio'] = $this->session->userdata('user_id_condominio');
            $this->mensagens->do_insert($dados);
        endif;
        set_tema('titulo', 'Enviar mensagem');
        set_tema('conteudo', load_modulo('mensagens', 'enviar'));
        load_template();
    }

    public function pega_qtde_mensagens(){
        echo $this->mensagens->get_qtde();
    }

}
