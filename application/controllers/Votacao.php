<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Votacao extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        init_painel();
        $this->load->model('votacao_model', 'votacao');
        init_datatables();
        set_tema('css_head', load_css(array('votacao')), FALSE);
        set_tema('footerinc', load_js(array('funcoes_votacao')), FALSE);
        set_tema('logo', carrega_logo($this->session->userdata('user_id_condominio')));
        set_tema('acesso', gera_acesso());
        set_tema('menu', gera_menu());
        set_tema('info_votacoes', gera_alertas());
    }

    public function index() {
        $this->gerenciar();
    }

    public function gerenciar() {
        esta_logado();
        set_tema('titulo', 'Votações');
        set_tema('conteudo', load_modulo('votacao', 'gerenciar'));
        load_template();
    }
    
    public function cadastrar(){
        if ($this->session->userdata('user_nivel') === '1'){
            $this->form_validation->set_rules('titulo', 'Titulo', 'trim|required|ucwords');
            if ($this->form_validation->run()==TRUE):
                $dados['titulo']        = mb_strtoupper($this->input->post('titulo'), 'UTF-8');
                $dados['id_usuario']    = $this->session->userdata('user_id');
                $dados['id_condominio'] = $this->session->userdata('user_id_condominio');
                $this->votacao->do_insert($dados);
            endif;
            set_tema('titulo', 'Cadastro de votação');
            set_tema('conteudo', load_modulo('votacao', 'cadastrar'));
            load_template();
        }else{
            redirect('votacao');
        }
    }

    public function editar(){
        if ($this->session->userdata('user_nivel') === '1'){
            $this->form_validation->set_rules('titulo', 'Titulo', 'trim|required|ucwords');
            if ($this->form_validation->run()==TRUE):
                $dados['titulo'] = $this->input->post('titulo');
                $this->votacao->do_update($dados, array('id'=>$this->input->post('id_votacao')));
            endif;
            set_tema('titulo', 'Alteração de votação');
            set_tema('conteudo', load_modulo('votacao', 'editar'));
            load_template();
        }else{
            redirect('votacao');
        }
        
    }
	
    public function excluir(){
        if ($this->session->userdata('user_nivel') === '1'):
            $id_votacao = $this->uri->segment(3);
            if ($id_votacao != NULL):
                $query = $this->votacao->get_byid($id_votacao);
                if ($query->num_rows()==1):
                    $query = $query->row();
                    if ($query->id != 1):
                        $this->votacao->do_delete(array('id'=>$query->id), FALSE);
                    else:
                        set_msg('msgerro', 'Esta votação não pode ser excluído', 'erro');
                    endif;
                else:
                        set_msg('msgerro', 'Votação não encontrado para exclusão', 'erro');
                endif;
            else:
                set_msg('msgerro', 'Escolha um votação para excluir', 'erro');
            endif;
        endif;
        redirect('votacao');
    }
}