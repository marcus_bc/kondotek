<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Painel extends CI_Controller {

        public function __construct() {
            parent::__construct();
            init_painel();
            set_tema('logo', carrega_logo($this->session->userdata('user_id_condominio')));
            $this->load->model('painel_model', 'painel');
        }

        public function index() {
            if($this->session->userdata('user_nivel') === '0'){
                $this->inicio_condomino();
            }elseif($this->session->userdata('user_nivel') === '1'){
                $this->inicio_sindico();
            }elseif($this->session->userdata('user_nivel') === '2'){
                $this->inicio_administradora();
            }elseif($this->session->userdata('user_nivel') === '3'){
                $this->inicio_portaria();
            }else{
                redirect('login');
            }
	}

        public function inicio_condomino() {
            esta_logado();
            set_tema('css_head', load_css(array('condomino/painel')), FALSE);
            set_tema('css_head', load_css(array('owl.carousel')), FALSE);
            set_tema('css_head', load_css(array('owl.theme.default.min')), FALSE);
            set_tema('footerinc', load_js(array('libs/owl.carousel.min')), FALSE);
            set_tema('footerinc', load_js(array('condomino/funcoes_painel')), FALSE);
            set_tema('titulo', 'Inicio');
            set_tema('acesso', '<div class="acesso"><p>'.$this->session->userdata('user_nome').'</p></div>');
            set_tema('menu', gera_menu());
            set_tema('info_votacoes', gera_alertas());
            set_tema('conteudo', load_modulo('condomino/painel', 'gerenciar'));
            load_template();
        }

        public function inicio_sindico() {
            esta_logado();
            set_tema('css_head', load_css(array('sindico/painel')), FALSE);
            set_tema('footerinc', load_js(array('sindico/funcoes_painel')), FALSE);
            set_tema('titulo', 'Inicio');
            set_tema('acesso', '<div class="acesso"><p>'.$this->session->userdata('user_nome').'</p></div>');
            set_tema('menu', gera_menu());
            set_tema('info_votacoes', gera_alertas());
            set_tema('conteudo', load_modulo('sindico/painel', 'gerenciar'));
            load_template();
        }

        public function inicio_administradora() {
            esta_logado();
            set_tema('css_head', load_css(array('administradora/painel')), FALSE);
            set_tema('footerinc', load_js(array('administradora/funcoes_painel')), FALSE);
            set_tema('titulo', 'Inicio');
            set_tema('acesso', '<div class="acesso"><p>'.$this->session->userdata('user_nome').'</p></div>');
            set_tema('menu', gera_menu());
            set_tema('info_votacoes', gera_alertas());
            set_tema('conteudo', load_modulo('administradora/painel', 'gerenciar'));
            load_template();
        }

        public function inicio_portaria() {
            esta_logado();
            set_tema('css_head', load_css(array('portaria/painel')), FALSE);
            set_tema('footerinc', load_js(array('portaria/funcoes_painel')), FALSE);
            set_tema('titulo', 'Inicio');
            set_tema('acesso', '<div class="acesso"><p>'.$this->session->userdata('user_nome').'</p></div>');
            set_tema('menu', gera_menu());
            set_tema('info_votacoes', gera_alertas());
            set_tema('conteudo', load_modulo('portaria/painel', 'gerenciar'));
            load_template();
        }

        public function atualiza_status_aviso() {
            echo $this->painel->do_update_status_aviso($this->input->post('id_aviso'));
        }
}
