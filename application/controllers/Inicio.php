<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @brief controller inicio
 * @author Marcus Vinicius Brum da Costa <marcus@kondotek.com.br>
 * @date   22/09/2015
 */

class Inicio extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        init_painel();
        set_tema('logo', carrega_logo());
        set_tema('css_head', load_css(array('inicio')), FALSE);
        //set_tema('footerinc', load_js(array('funcoes_login')), FALSE);
        //set_tema('js_head', '');
        //$this->load->model('login_model', 'login');
        //$this->load->library('user_agent');
    }
    
    public function index() {
        if(esta_logado(FALSE)){
            redirect('painel');
        }
        //set_tema('titulo', '');
        set_tema('conteudo', load_modulo('inicio', 'inicio'));
        load_template();
        //$this->efetuar_login();
    }
}