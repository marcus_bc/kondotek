<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Documentos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_painel();
        $this->load->model('documentos_model', 'documentos');
        init_datatables();
        set_tema('css_head', load_css(array('documentos')), FALSE);
        set_tema('footerinc', load_js(array('funcoes_documentos')), FALSE);
        set_tema('logo', carrega_logo($this->session->userdata('user_id_condominio')));
        set_tema('acesso', gera_acesso());
        set_tema('menu', gera_menu());
        set_tema('info_votacoes', gera_alertas());
    }

    public function index() {
        $this->gerenciar();
    }

    public function gerenciar() {
        esta_logado();
        set_tema('titulo', 'Documentos');
        set_tema('conteudo', load_modulo('documentos', 'gerenciar'));
        load_template();
    }

}
