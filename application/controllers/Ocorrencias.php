<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ocorrencias extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_painel();
        $this->load->model('ocorrencias_model', 'ocorrencias');
        init_datatables();
        set_tema('css_head', load_css(array('ocorrencias')), FALSE);
        set_tema('footerinc', load_js(array('funcoes_ocorrencias')), FALSE);
        set_tema('logo', carrega_logo($this->session->userdata('user_id_condominio')));
        set_tema('acesso', gera_acesso());
        set_tema('menu', gera_menu());
        set_tema('info_votacoes', gera_alertas());
    }

    public function index() {
        $this->gerenciar();
    }

    public function gerenciar() {
        esta_logado();
        set_tema('titulo', 'Ocorrências');
        set_tema('conteudo', load_modulo('ocorrencias', 'gerenciar'));
        load_template();
    }
    
    public function pega_ocorrencias(){
        echo $this->ocorrencias->get_ocorrencias();
    }

    public function cadastrar(){
        if ($this->session->userdata('user_nivel') === '1'){
            if(strlen($this->input->post('texto')) < 3){
                echo json_encode('-2');
                return false;
            }
            $dados['texto']         = mb_strtoupper($this->input->post('texto'), 'UTF-8');
            $dados['id_usuario']    = $this->session->userdata('user_id');
            $dados['id_condominio'] = $this->session->userdata('user_id_condominio');
            echo $this->ocorrencias->do_insert($dados);
        }
    }

    public function editar(){
        if ($this->session->userdata('user_nivel') === '1'){
            $this->form_validation->set_rules('texto', 'Texto', 'trim|required|ucwords');
            if ($this->form_validation->run()==TRUE):
                $dados['texto'] = $this->input->post('texto');
                $this->ocorrencias->do_update($dados, array('id'=>$this->input->post('id_ocorrencia')));
            endif;
            set_tema('titulo', 'Alteração de ocorrência');
            set_tema('conteudo', load_modulo('ocorrencias', 'editar'));
            load_template();
        }else{
            redirect('ocorrencias');
        }

    }

    public function excluir(){
        if ($this->session->userdata('user_nivel') === '1'):
            $id_ocorrencia = $this->uri->segment(3);
            if ($id_ocorrencia != NULL):
                $query = $this->ocorrencias->get_byid($id_ocorrencia);
                if ($query->num_rows()==1):
                    $query = $query->row();
                    if ($query->id != 1):
                        $this->ocorrencias->do_delete(array('id'=>$query->id), FALSE);
                    else:
                        set_msg('msgerro', 'Esta ocorrência não pode ser excluída', 'erro');
                    endif;
                else:
                        set_msg('msgerro', 'Ocorrência não encontrado para exclusão', 'erro');
                endif;
            else:
                set_msg('msgerro', 'Escolha uma ocorrência para excluir', 'erro');
            endif;
        endif;
        redirect('ocorrencias');
    }

    public function pega_qtde_ocorrencias(){
        echo $this->ocorrencias->get_qtde();
    }
}
