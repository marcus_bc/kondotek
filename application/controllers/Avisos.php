<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Avisos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        init_painel();
        $this->load->model('avisos_model', 'avisos');
        init_datatables();
        set_tema('css_head', load_css(array('avisos')), FALSE);
        set_tema('footerinc', load_js(array('funcoes_avisos')), FALSE);
        set_tema('logo', carrega_logo($this->session->userdata('user_id_condominio')));
        set_tema('acesso', gera_acesso());
        set_tema('menu', gera_menu());
        set_tema('info_votacoes', gera_alertas());
    }

    public function index() {
        $this->gerenciar();
    }

    public function gerenciar() {
        esta_logado();
        set_tema('titulo', 'Avisos');
        set_tema('conteudo', load_modulo('avisos', 'gerenciar'));
        load_template();
    }
    
    public function pega_avisos(){
        echo $this->avisos->get_avisos();
    }

    public function cadastrar(){
        if ($this->session->userdata('user_nivel') === '1'){
            if(strlen($this->input->post('titulo')) < 3){
                echo json_encode('-2');
                return false;
            }
            if(strlen($this->input->post('texto')) < 3){
                echo json_encode('-3');
                return false;
            }
            $dados['titulo']        = mb_strtoupper($this->input->post('titulo'), 'UTF-8');
            $dados['texto']         = mb_strtoupper($this->input->post('texto'), 'UTF-8');
            $dados['id_usuario']    = $this->session->userdata('user_id');
            $dados['id_condominio'] = $this->session->userdata('user_id_condominio');
            echo $this->avisos->do_insert($dados);
        }
    }

    public function editar(){
        if ($this->session->userdata('user_nivel') === '1'){
            $this->form_validation->set_rules('titulo', 'Titulo', 'trim|required|ucwords');
            if ($this->form_validation->run()==TRUE):
                $dados['titulo'] = $this->input->post('titulo');
                $dados['texto'] = $this->input->post('texto');
                $this->avisos->do_update($dados, array('id'=>$this->input->post('id_aviso')));
            endif;
            set_tema('titulo', 'Alteração de aviso');
            set_tema('conteudo', load_modulo('avisos', 'editar'));
            load_template();
        }else{
            redirect('avisos');
        }

    }

    public function excluir(){
        if ($this->session->userdata('user_nivel') === '1'):
            $id_aviso = $this->uri->segment(3);
            if ($id_aviso != NULL):
                $query = $this->avisos->get_byid($id_aviso);
                if ($query->num_rows()==1):
                    $query = $query->row();
                    if ($query->id != 1):
                        $this->avisos->do_delete(array('id'=>$query->id), FALSE);
                    else:
                        set_msg('msgerro', 'Este aviso não pode ser excluído', 'erro');
                    endif;
                else:
                        set_msg('msgerro', 'Aviso não encontrado para exclusão', 'erro');
                endif;
            else:
                set_msg('msgerro', 'Escolha um aviso para excluir', 'erro');
            endif;
        endif;
        redirect('avisos');
    }

}
