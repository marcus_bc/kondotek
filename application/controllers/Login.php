<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @brief controller login
 * @author Marcus Vinicius Brum da Costa <marcus@kondotek.com.br>
 * @date   30/08/2015
 */

class Login extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        init_painel();
        set_tema('css_head', load_css(array('login')), FALSE);
        set_tema('footerinc', load_js(array('funcoes_login')), FALSE);
        set_tema('js_head', '');
        $this->load->model('login_model', 'login');
        //$this->load->library('user_agent');
    }
    
    public function index() {
        if(esta_logado(FALSE)){
            redirect('painel');
        }
        $this->form_validation->set_rules('email', 'E-MAIL', 'trim|required|valid_email');
        $this->form_validation->set_rules('senha', 'SENHA', 'trim|required|min_length[4]|strtolower');
        if($this->form_validation->run()==TRUE){
            /*
            if(!$this->agent->is_browser('Chrome')){
                set_msg('errologin', 'Utilize o Google Chrome para acessar o sistema!', 'erro');
                redirect('usuarios/login');
            }
            */
            $email = $this->input->post('email', TRUE);
            $senha = $this->input->post('senha', TRUE);
            $redirect = $this->input->post('redirect', TRUE);
            if($this->login->do_login($email, $senha) == TRUE){
                $query = $this->login->get_byemail($email)->row();
                $condominio_nome = $this->login->get_condominio_nome($query->id_condominio)->row();
                if($condominio_nome->ativo !== '1' ){
                    set_msg('errologin', 'Seu condomínio está inativo', 'erro');
                    redirect('login');
                }
                $dados = array(
                    'user_id' => $query->id,
                    'user_nome' => $query->nome,
                    'user_id_condominio' => $query->id_condominio,
                    'user_condominio' => $condominio_nome->nome,
                    'user_logado' => TRUE,
                    'user_nivel' => $query->nivel_acesso,
                );
                $this->session->set_userdata($dados);
                //auditoria('Login no sistema', 'Login efetuado com sucesso');
                if($redirect != ''){
                    redirect($redirect);
                }else{
                    redirect('login');
                }
            }else{
                $query = $this->login->get_byemail($email)->row();
                if(empty($query)){
                    set_msg('errologin', 'Usuário inexistente', 'erro');
                }elseif($query->senha != $senha){
                    set_msg('errologin', 'Senha incorreta', 'erro');
                }elseif($query->ativo == 0){
                    set_msg('errologin', 'Este usuário está inativo', 'erro');
                }else{
                    set_msg('errologin', 'Erro desconhecido', 'erro');
                }
                redirect('login');
            }
        }
        set_tema('titulo', 'login');
        set_tema('conteudo', load_modulo('login', 'login'));
        load_template();
        //$this->efetuar_login();
    }
    
    public function nova_senha(){
        $this->form_validation->set_rules('email', 'EMAIL', 'trim|required|valid_email|strtolower');
        if($this->form_validation->run()==TRUE){
            $email = $this->input->post('email');
            $query = $this->login->get_byemail($email);
            if($query->num_rows()==1){
                $novasenha = substr(str_shuffle('qwertyuiopasdfghjklzxcvbnm0123456789'), 0, 6);
                $mensagem = "<p>Você solicitou uma nova senha para acesso ao sistema RFLY, 
                    a partir de agora use a seguinte senha para acesso: <strong>$novasenha</strong></p>
                    <p>Troque esta senha para uma senha segura e de sua preferência o quanto antes.</p>";
                if($this->sistema->enviar_email($email, 'Nova senha de acesso', $mensagem)){
                    //$dados['senha'] = md5($novasenha);
                    $dados['senha'] = password_hash($novasenha, PASSWORD_DEFAULT);
                    $this->usuarios->do_update($dados, array('email'=>$email), FALSE);
                    //auditoria('Redefinição de senha', 'O usuário solicitou uma nova senha por email');
                    set_msg('msgok', 'Uma nova senha foi enviada para seu email', 'sucesso');
                    redirect('login/nova_senha');
                }else{
                    set_msg('msgerro', 'Erro ao enviar nova senha, contate o administrador', 'erro');
                    redirect('login/nova_senha');
                }
            }else{
                set_msg('msgerro', 'Este email não possui cadastro no sistema', 'erro');
                redirect('login/nova_senha');
            }
        }
        set_tema('titulo', 'Recuperar senha');
        set_tema('conteudo', load_modulo('login', 'nova_senha'));
        set_tema('rodape', '<div id="rodape"><span>&copy; 2014 | Todos os direitos reservados para RFLY</span></div>');
        load_template();
    }
    
    public function logoff(){
        $this->session->unset_userdata();
        $this->session->sess_destroy();
        redirect('login');
    }

}