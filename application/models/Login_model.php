<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @brief model login_model
 * @author Marcus Vinicius Brum da Costa <marcus@kondotek.com.br>
 * @date   30/08/2015
 */

class Login_model extends CI_Model {
    
    public function do_login($email=NULL, $senha=NULL){
        if($email && $senha){
            $this->db->where('email', $email);
            $this->db->where('ativo', '1');
            $query = $this->db->get('usuarios');
            foreach ($query->result() as $row){
                if(password_verify($senha, $row->senha)== TRUE){
                    return TRUE;
                }else{
                    return FALSE;
                }
            }
        }else{
            return FALSE;
        }
    }
    
    public function get_byemail($email=NULL){
        if($email != NULL){
            $this->db->where('email', $email);
            $this->db->where('ativo', '1');
            $this->db->limit(1);
            return $this->db->get('usuarios');
        }else{
            return FALSE;
        }
    }
    
    public function get_condominio_nome($condominio_id=NULL){
        if($condominio_id != NULL){
            $this->db->select('id AS id_condominio, nome, ativo');
            $this->db->where('id', $condominio_id);
            $this->db->limit(1);
            return $this->db->get('condominio');
        }else{
            return FALSE;
        }
    }
}