<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @brief model votacao_model
 * @author Marcus Vinicius Brum da Costa <marcus@kondotek.com.br>
 * @date   22/09/2015
 */

class Votacao_model extends CI_Model {
    
    public function do_insert($dados=NULL, $redir=TRUE){
        if ($dados != NULL):
            $this->db->insert('votacao', $dados);
            if ($this->db->affected_rows()>0):
                //auditoria('Inclusão de usuários', 'Usuário "'.$dados['login'].'" cadastrado no sistema');
                set_msg('msgok', 'Cadastro efetuado com sucesso', 'sucesso');
            else:
                set_msg('msgerro', 'Erro ao inserir dados', 'erro');
            endif;
            if ($redir) redirect(current_url());
        endif;
    }
	
    public function do_update($dados=NULL, $condicao=NULL, $redir=TRUE){
        if ($dados != NULL && is_array($condicao)):
            $this->db->update('votacao', $dados, $condicao);
            if ($this->db->affected_rows()>0):
                set_msg('msgok', 'Alteração efetuada com sucesso', 'sucesso');
            else:
                set_msg('msgerro', 'Erro ao atualizar dados', 'erro');
            endif;
            if ($redir) redirect(current_url());
        endif;
    }
	
    public function do_delete($condicao=NULL, $redir=TRUE){
        if ($condicao != NULL && is_array($condicao)):
            $dados['ativo'] = FALSE;
            $this->db->update('votacao', $dados, $condicao);
            if ($this->db->affected_rows()>0):
                //auditoria('Exclusão de usuários', 'Excluído cadastro do usuário "'.$usuario.'"');
                set_msg('msgok', 'Registro excluído com sucesso', 'sucesso');
            else:
                set_msg('msgerro', 'Erro ao excluir registro', 'erro');
            endif;
            if ($redir) redirect(current_url());
        endif;
    }

    public function get_byid($id=NULL){
        if ($id != NULL):
            $this->db->where('id_condominio', $this->session->userdata('user_id_condominio'));
            $this->db->where('ativo', TRUE);
            $this->db->where('id', $id);
            $this->db->limit(1);
            return $this->db->get('votacao');
        else:
            return FALSE;
        endif;
    }

    public function get_all(){
        $this->db->where('ativo', TRUE);
        $this->db->where('id_condominio', $this->session->userdata('user_id_condominio'));
        return $this->db->get('votacao');
    }
    
    public function get_count_ativas(){
        $this->db->select('count(id) AS qtde');
        $this->db->where('ativo', TRUE);
        $this->db->where('finalizada', FALSE);
        $this->db->where('id_condominio', $this->session->userdata('user_id_condominio'));
        return $this->db->get('votacao');
    }
}