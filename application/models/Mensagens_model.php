<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @brief model mensagens_model
 * @author Marcus Vinicius Brum da Costa <marcus@kondotek.com.br>
 * @date   06/10/2015
 */

class Mensagens_model extends CI_Model {

    public function do_insert($dados=NULL, $redir=TRUE){
        if ($dados != NULL):
            $this->db->trans_begin();
            $this->db->insert('mensagens', $dados);
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                set_msg('msgerro', 'Erro ao enviar mensagem', 'erro');
            }else{
                $this->db->trans_commit();
                set_msg('msgok', 'Mensagem enviada com sucesso', 'sucesso');
            }
            if ($redir) redirect(current_url());
        endif;
    }

    public function get_byid($id=NULL){
        if ($id != NULL):
            $this->db->where('id_condominio', $this->session->userdata('user_id_condominio'));
            $this->db->where('ativo', TRUE);
            $this->db->where('id', $id);
            $this->db->limit(1);
            return $this->db->get('mensagens');
        else:
            return FALSE;
        endif;
    }

    public function get_all(){
        $this->db->where('ativo', TRUE);
        $this->db->where('id_condominio', $this->session->userdata('user_id_condominio'));
        return $this->db->get('mensagens');
    }

    public function get_qtde(){
        $this->db->select('count(id) AS qtde');
        $this->db->where('ativo', TRUE);
        $this->db->where('id_condominio', $this->session->userdata('user_id_condominio'));
        $query = $this->db->get('mensagens');
        if ($query->num_rows() > 0){
            return json_encode($query->result());
        }
    }
}
