<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @brief model avisos_model
 * @author Marcus Vinicius Brum da Costa <marcus@kondotek.com.br>
 * @date   14/09/2015
 */

class Avisos_model extends CI_Model {

    public function do_insert($dados=NULL){
        if ($dados != NULL){
            $this->db->trans_begin();
            $this->db->insert('avisos', $dados);
            if ($this->db->affected_rows()>0){
                $id_aviso                              = $this->db->insert_id();
                $dados_avisos_usuario['id_aviso']      = $id_aviso;
                $dados_avisos_usuario['id_usuario']    = $this->session->userdata('user_id');
                $dados_avisos_usuario['id_condominio'] = $this->session->userdata('user_id_condominio');
                $this->db->insert('avisos_usuario', $dados_avisos_usuario);
            }
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                return json_encode('-1');
                //set_msg('msgerro', 'Erro ao inserir dados', 'erro');
            }else{
                $this->db->trans_commit();
                return json_encode($id_aviso);
                //set_msg('msgok', 'Cadastro efetuado com sucesso', 'sucesso');
            }
        }
    }

    public function do_update($dados=NULL, $condicao=NULL, $redir=TRUE){
        if ($dados != NULL && is_array($condicao)):
            $this->db->update('avisos', $dados, $condicao);
            if ($this->db->affected_rows()>0):
                set_msg('msgok', 'Alteração efetuada com sucesso', 'sucesso');
            else:
                set_msg('msgerro', 'Erro ao atualizar dados', 'erro');
            endif;
            if ($redir) redirect(current_url());
        endif;
    }

    public function do_delete($condicao=NULL, $redir=TRUE){
        if ($condicao != NULL && is_array($condicao)):
            $dados['ativo'] = FALSE;
            $this->db->update('avisos', $dados, $condicao);
            if ($this->db->affected_rows()>0):
                //auditoria('Exclusão de usuários', 'Excluído cadastro do usuário "'.$usuario.'"');
                set_msg('msgok', 'Registro excluído com sucesso', 'sucesso');
            else:
                set_msg('msgerro', 'Erro ao excluir registro', 'erro');
            endif;
            if ($redir) redirect(current_url());
        endif;
    }

    public function get_byid($id=NULL){
        if ($id != NULL):
            $this->db->where('id_condominio', $this->session->userdata('user_id_condominio'));
            $this->db->where('ativo', TRUE);
            $this->db->where('id', $id);
            $this->db->limit(1);
            return $this->db->get('avisos');
        else:
            return FALSE;
        endif;
    }
    /*
    public function get_all(){
        $this->db->where('ativo', TRUE);
        $this->db->where('id_condominio', $this->session->userdata('user_id_condominio'));
        return $this->db->get('avisos');
    }
    */
    public function get_avisos(){
        $this->db->where('ativo', TRUE);
        $this->db->where('id_condominio', $this->session->userdata('user_id_condominio'));
        $query = $this->db->get('avisos');
        if ($query->num_rows() > 0){
            return json_encode($query->result());
        }
    }
}
