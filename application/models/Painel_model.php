<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @brief model painel_model
 * @author Marcus Vinicius Brum da Costa <marcus@kondotek.com.br>
 * @date   09/089/2015
 */

class Painel_model extends CI_Model {
    
    public function get_all_avisos(){
        $this->db->select('avisos.id, avisos.titulo, avisos.texto,
                           to_char(avisos.data_cadastro, \'DD/MM/YYYY HH24:MI:SS\') AS data_cadastro,
                           avisos_usuario.visualizado', FALSE);
        $this->db->where('avisos.ativo', TRUE);
        $this->db->where('avisos.id_condominio', $this->session->userdata('user_id_condominio'));
        $this->db->from('avisos');
        $this->db->join('avisos_usuario', 'avisos_usuario.id_aviso = avisos.id', 'left');
        $this->db->order_by('avisos.id', 'DESC');
        $this->db->limit(8);
        return $this->db->get();
    }
    
    public function get_all_votacao(){
        $this->db->where('ativo', TRUE);
        $this->db->where('id_condominio', $this->session->userdata('user_id_condominio'));
        $this->db->order_by('id', 'DESC');
        return $this->db->get('votacao');
    }
    
    public function do_update_status_aviso($id_aviso=NULL){
        if($id_aviso){
            $dados['visualizado']       = TRUE;
            $condicao['id_aviso']             = (int) $id_aviso;
            $condicao['id_condominio']  = $this->session->userdata('user_id_condominio');
            $this->db->update('avisos_usuario', $dados, $condicao);
            if($this->db->affected_rows()>0){
                return json_encode($id_aviso);
            }else{
                return json_encode($this->db->last_query());
            }
        }
    }
}