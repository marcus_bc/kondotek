/**
 * @brief js funcoes_painel_condomino
 * @author Marcus <marcus@kondotek.com.br>
 * @date   30/09/2015
 */

$(document).ready(function(){
    /*[INI] Declaração de variaveis*/
    var texto_avisos = $('.texto_avisos');


    /*[FIM] Declaração de variaveis*/

    /*[INI] Event Listeners*/
    texto_avisos.on('click', function(){
        var id_aviso = $(this).find('.id_aviso').val();
        $.ajax({
            url:"painel/atualiza_status_aviso",
            type:"post",
            data:{id_aviso:id_aviso},
            dataType:'json',
            success:function(){
                alert(texto_avisos.html());
            }
        });
    });

    $(".teste").owlCarousel({
        items:1,
        margin:10,
        nav:true,
        autoHeight:true
    });

    /*[FIM] Event Listeners*/

    /*[INI] Callbacks*/

   /*[FIM] Callbacks*/
});
