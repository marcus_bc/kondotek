/**
 * @brief js funcoes_painel_sindico
 * @author Marcus <marcus@kondotek.com.br>
 * @date   15/10/2015
 */

$(document).ready(function(){
    /*[INI] Declaração de variaveis*/
    var btn_mensagens   = $('.btn_mensagens');
    var btn_ocorrencias = $('.btn_ocorrencias');
    var btn_criar_aviso = $('#btn_criar_aviso');
    var html_cadastro   = $('#cadastrar_aviso');


    /*[FIM] Declaração de variaveis*/

    /*[INI] Event Listeners*/
    btn_criar_aviso.on('click', function(){
        noty({
            text        : html_cadastro.html(),
            dismissQueue: true,
            layout      : 'center',
            theme       : 'defaultTheme',
            modal       : true,
            callback: {
                afterShow: function() {
                    $('.noty_text :input:first').focus();
                }
            },
            buttons : [
                {addClass: 'btn btn-primary', text: 'Salvar', onClick: function ($noty) {
                    var titulo  = $('.noty_text').find('.input_titulo').val();
                    var texto   = $('.noty_text').find('.input_texto').val();
                    $.ajax({
                        url:"avisos/cadastrar",
                        type:"post",
                        data:{titulo:titulo, texto:texto},
                        dataType:'json',
                        success:function(retorno){
                            $noty.close();
                            if(retorno === '-1'){
                                alerta('error', 'Alerta', 2000, 'center');
                            }else if (retorno === '-2'){
                                alerta('error', 'Titulo: Minimo 3 caracteres', 2000, 'center');
                            }else if (retorno === '-3'){
                                alerta('error', 'Texto: Minimo 3 caracteres', 2000, 'center');
                            }else{
                                alerta('success', 'Salvo', 2000, 'center');
                                tb_avisos.DataTable().ajax.reload();
                            }
                        },
                        fail:function(){
                            alerta('error', 'Alerta', 2000, 'center');
                        }
                    });
                }
                },
                {addClass: 'btn btn-danger', text: 'Cancelar', onClick: function ($noty) {
                    $noty.close();
                }
                }
            ]
        });
    });

    /*[FIM] Event Listeners*/

    /*[INI] Callbacks*/
    //Exibe a quantidade de mensagens
    $.ajax({
        url:"mensagens/pega_qtde_mensagens",
        type:"get",
        dataType:'json',
        success:function(qtde){
            btn_mensagens.addClass('qtde').attr('qtde_mensagens', qtde[0].qtde);
        }
    });
    //Exibe a quantidade de ocorrencias
    $.ajax({
        url:"ocorrencias/pega_qtde_ocorrencias",
        type:"get",
        dataType:'json',
        success:function(qtde){
            btn_ocorrencias.addClass('qtde').attr('qtde_ocorrencias', qtde[0].qtde);
        }
    });
   /*[FIM] Callbacks*/
});
