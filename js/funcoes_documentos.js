/**
 * @brief js funcoes_documentos
 * @author Wilson <wilson@kondotek.com.br>
 * @date   10/04/2016
 */

$(document).ready(function(){
    /*[INI] Declaração de variaveis*/
    var tb_documentos   = $('#tb_documentos');
    var html_cadastro   = $('#cadastrar_documento');
    var cad_documento   = $('.cad_documento');

    /*[FIM] Declaração de variaveis*/

    /*[INI] Event Listeners*/
    tb_documentos.DataTable( {
        ajax: {
            url    : 'documentos/pega_documentos',
            dataSrc: ''
        },
        'columns': [
            { 'data': 'titulo' },
            { 'data': 'arquivo' }
        ]
    } );

    /*[FIM] Event Listeners*/

    /*[INI] Callbacks*/

   /*[FIM] Callbacks*/
});
