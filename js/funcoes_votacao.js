/**
 * @brief js funcoes_votacao
 * @author Marcus <marcus@kondotek.com.br>
 * @date   22/09/2015
 */

$(document).ready(function(){
    /*[INI] Declaração de variaveis*/
    var tb_votacao = $('#tb_votacao');
    
    
    /*[FIM] Declaração de variaveis*/
    
    /*[INI] Event Listeners*/
    tb_votacao.DataTable( {
        "language": {
            "url": "js/datatable_language_PT-BR.json"
        }
    } );
    
    /*[FIM] Event Listeners*/
    
    /*[INI] Callbacks*/

   /*[FIM] Callbacks*/
});