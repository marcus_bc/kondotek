/**
 * @brief js funcoes_ocorrencias
 * @author Marcus <marcus@kondotek.com.br>
 * @date   04/10/2015
 */

$(document).ready(function(){
    /*[INI] Declaração de variaveis*/
    var tb_ocorrencias = $('#tb_ocorrencias');
    var html_cadastro  = $('#cadastrar_ocorrencia');
    var cad_ocorrencia = $('.cad_ocorrencia');

    /*[FIM] Declaração de variaveis*/

    /*[INI] Event Listeners*/
    tb_ocorrencias.DataTable( {
        ajax: {
            url    : 'ocorrencias/pega_ocorrencias',
            dataSrc: ''
        },
        "columns": [
            { "data": "texto"}
        ],


    } );

    cad_ocorrencia.on('click', function(){
       noty({
            text        : html_cadastro.html(),
            dismissQueue: true,
            layout      : 'center',
            theme       : 'defaultTheme',
            modal       : true,
            callback: {
                afterShow: function() {
                    $('.noty_text :input:first').focus();
                }
            },
            buttons : [
                {addClass: 'btn btn-primary', text: 'Salvar', onClick: function ($noty) {
                    var texto   = $('.noty_text').find('.input_texto').val();
                    $.ajax({
                        url:"ocorrencias/cadastrar",
                        type:"post",
                        data:{texto:texto},
                        dataType:'json',
                        success:function(retorno){
                            $noty.close();
                            if(retorno === '-1'){
                                alerta('error', 'Alerta', 2000, 'center');
                            }else if (retorno === '-2'){
                                alerta('error', 'Texto: Minimo 3 caracteres', 2000, 'center');
                            }else{
                                alerta('success', 'Salvo', 2000, 'center');
                                tb_ocorrencias.DataTable().ajax.reload();
                            }
                        },
                        fail:function(){
                            alerta('error', 'Alerta', 2000, 'center');
                        }
                    });
                }
                },
                {addClass: 'btn btn-danger', text: 'Cancelar', onClick: function ($noty) {
                    $noty.close();
                }
                }
            ]
        });
    });

    /*[FIM] Event Listeners*/

    /*[INI] Callbacks*/

   /*[FIM] Callbacks*/
});
