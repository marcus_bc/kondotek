/**
 * @brief js noty
 * @author Marcus <marcus@kondotek.com.br>
 * @date   11/10/2015
 */

/*  EX:
    var alert = alerta('alert', 'Alerta', 500);
    var information = alerta('information', 'Informação');
    var error = alerta('error', 'Erro');
    var warning = alerta('warning', 'Aviso');
    var notification = alerta('notification', 'Notificação');
    var success = alerta('success', 'Sucesso');
 */
function alerta(type, text, close, position) {
    position = typeof position !== 'undefined' ?  position : 'topRight';
    close = typeof close !== 'undefined' ?  close : false;
    var n = noty({
        text        : text,
        type        : type,
        dismissQueue: false,
        layout      : position,
        theme       : 'defaultTheme',
        timeout     : close
    });
    console.log(type + ' - ' + n.options.id);
    return n;
}
