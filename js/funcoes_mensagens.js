/**
 * @brief js funcoes_mensagens
 * @author Marcus <marcus@kondotek.com.br>
 * @date   06/10/2015
 */

$(document).ready(function(){
    /*[INI] Declaração de variaveis*/
    var tb_mensagens = $('#tb_mensagens');
    
    
    /*[FIM] Declaração de variaveis*/
    
    /*[INI] Event Listeners*/
    tb_mensagens.DataTable( {
        "language": {
            "url": "js/datatable_language_PT-BR.json"
        }
    } );
    
    /*[FIM] Event Listeners*/
    
    /*[INI] Callbacks*/

   /*[FIM] Callbacks*/
});