/**
 * @brief js funcoes_avisos
 * @author Marcus <marcus@kondotek.com.br>
 * @date   15/09/2015
 */

$(document).ready(function(){
    /*[INI] Declaração de variaveis*/
    var tb_avisos       = $('#tb_avisos');
    var html_cadastro   = $('#cadastrar_aviso');
    var cad_aviso       = $('.cad_aviso');

    /*[FIM] Declaração de variaveis*/

    /*[INI] Event Listeners*/
    tb_avisos.DataTable( {
        ajax: {
            url    : 'avisos/pega_avisos',
            dataSrc: ''
        },
        "columns": [
            { "data": "titulo" },
            { "data": "texto" }
        ]
    } );

    cad_aviso.on('click', function(){
       noty({
            text        : html_cadastro.html(),
            dismissQueue: true,
            layout      : 'center',
            theme       : 'defaultTheme',
            modal       : true,
            callback: {
                afterShow: function() {
                    $('.noty_text :input:first').focus();
                }
            },
            buttons : [
                {addClass: 'btn btn-primary', text: 'Salvar', onClick: function ($noty) {
                    var titulo  = $('.noty_text').find('.input_titulo').val();
                    var texto   = $('.noty_text').find('.input_texto').val();
                    $.ajax({
                        url:"avisos/cadastrar",
                        type:"post",
                        data:{titulo:titulo, texto:texto},
                        dataType:'json',
                        success:function(retorno){
                            $noty.close();
                            if(retorno === '-1'){
                                alerta('error', 'Alerta', 2000, 'center');
                            }else if (retorno === '-2'){
                                alerta('error', 'Titulo: Minimo 3 caracteres', 2000, 'center');
                            }else if (retorno === '-3'){
                                alerta('error', 'Texto: Minimo 3 caracteres', 2000, 'center');
                            }else{
                                alerta('success', 'Salvo', 2000, 'center');
                                tb_avisos.DataTable().ajax.reload();
                            }
                        },
                        fail:function(){
                            alerta('error', 'Alerta', 2000, 'center');
                        }
                    });
                }
                },
                {addClass: 'btn btn-danger', text: 'Cancelar', onClick: function ($noty) {
                    $noty.close();
                }
                }
            ]
        });
    });

    /*[FIM] Event Listeners*/

    /*[INI] Callbacks*/

   /*[FIM] Callbacks*/
});
