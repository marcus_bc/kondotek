<?php
include_once '../config/conexao.php';
include_once '../config/funcoes.php';

//Página atual para menu
$pagina = "";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>

<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	
<meta http-equiv="Pragma" content="no-cache" />

<link rel="stylesheet" type="text/css" href="css/estilo.css" />

<title>Fotos - Kondotek</title>

</head>

<body>
	<div id="tudo">
		<?php include_once 'topo.php'; ?>
		<div id="conteudoSindico">
                <?php
                    if(isset($_GET['adicionar']) && $_GET['adicionar'] == TRUE){
                ?>
                <div style="float: left; font-family: arial; margin-left: 10px; margin-top: 5px;">
                    <a href="home.php">Início</a>
                    » 
                    <span style="color:#666666;"><strong>Adicionar Fotos</strong></span>
                </div>
                <br /><br />
                <div style="width: 70%; margin-top: 20px; margin-left: auto; margin-right: auto; font-family: arial; margin-bottom: 30px;">
                    <?php
                        $sql = "SELECT id, imagem, diretorio, data_cadastro, titulo, funcao";
                        $consulta = pg_query($con,$sql);
                        while ($linha = pg_fetch_assoc($consulta)){
                            $texto = $linha['texto'];
                            if($linha['nome'] != '' && $linha['nome'] != NULL){
                                $condomino = $linha['nome'];
                            }else{
                                $condomino = "Anônimo";
                            }
                            $data_cadastro = $linha['data_cadastro'];
                        }
                        echo "<h3>Data da ocorrência</h3>";
                        echo $data_cadastro;
                        echo "<br /><br />";
                        echo "<h3>Condômino</h3>";
                        echo $condomino;
                        echo "<br /><br />";
                        echo "<h3>Ocorrência</h3><br />";
                        echo $texto;
                        
                        //Atualiza status para visualizado
                        if($_GET['status'] == 0){
                            $sqlStatus = "UPDATE cad_ocorrencias
                                            SET status = '1'
                                          WHERE id = '".$_GET['id']."'
                                          AND id_condominio = '".$_SESSION['id_condominio']."'";
                            $consultaStatus = pg_query($con,$sqlStatus);
                        }
                    ?>  
                    
                </div>
                <?php }else{ ?>
                <div style="float: left; font-family: arial; margin-left: 10px; margin-top: 5px;">
                    <a href="home.php">Início</a>
                    » 
                    <span style="color:#666666;"><strong>Fotos</strong></span>
                </div>
                <br /><br />
                <div style="width: 70%; margin-top: 20px; margin-left: auto; margin-right: auto; font-family: arial;">
                    <table style="width: 100%;">
                        <thead>
                            <tr>
                                <th style='text-align:center;'>Data da ocorrência</th>
                                <th>Texto</th>
                                <th>Condômino</th>
                                <th style='text-align:center;'>Status</th>
                                <th style='text-align:center; width: 180px;'>Ação</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $sql = "SELECT co.id, co.texto, to_char(co.data_cadastro, 'DD/MM/YYYY') AS data_cadastro, co.status, cu.nome
                                        FROM cad_ocorrencias AS co
                                        left join cad_usuarios AS cu
                                        ON cu.id = co.id_usuario
                                        WHERE co.id_condominio = '".$_SESSION['id_condominio']."' 
                                        ORDER BY co.id DESC";
                                $consulta = pg_query($con,$sql);
                                while ($linha = pg_fetch_assoc($consulta)){
                                    echo "<tr>";
                                        echo "<td style='text-align:center;'>";
                                            echo $linha['data_cadastro'];
                                        echo "</td>";
                                        echo "<td>";
                                            echo limita_caracteres($linha['texto'],150);
                                        echo "</td>";
                                        echo "<td>";
                                            if($linha['nome'] != '' && $linha['nome'] != NULL){
                                                echo $linha['nome'];
                                            }else{
                                                echo "Anônimo";
                                            }
                                        echo "</td>";
                                        echo "<td style='text-align:center;'>";
                                            if($linha['status'] == 0){
                                                echo "<p style='color:#FF6600;'><strong>Não visualizada</strong></p>";
                                            }elseif($linha['status'] == 1){
                                                echo "Visualizada";
                                            }elseif($linha['status'] == 2){
                                                echo "<p style='color:green;'>Concluída</p>";
                                            }
                                        echo "</td>";
                                        echo "<td style='text-align:center;'>";
                                            echo "<a href='ocorrencias.php?visualizar=true&id=".$linha['id']."&status=".$linha['status']."' class='button radius small secondary'>Visualizar</a>&nbsp;&nbsp;&nbsp;<a href='ocorrencias.php?concluir=true&id=".$linha['id']."&status=".$linha['status']."' class='button radius small success'>Concluir</a>";
                                        echo "</td>";
                                    echo "</tr>";
                                }
                            
                            ?>
                        </tbody>
                    </table>
                </div>
                <?php } ?>
                </div>
            </div>
        <?php include_once 'rodape.php'; ?>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny.js"></script>
</body>
</html>