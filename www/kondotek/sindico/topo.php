<?php $pagina_atual = "class='ativo'" ?>
<div id="topo">
        <div id="logo"><img src="img/logo.png" alt="logo" /></div>
        <div id="topoInfoNome">
                <span class="titulo">
                    <?php
                        echo $_SESSION['nome'];
                    ?>
                </span>
                <br />
                <span class="subtitulo">Último Acesso - <?php echo $_SESSION['ultimo_acesso'];?></span>
                <div id="visualizarcomo">
                        <img src="img/btnvisualizarcomo.png" alt="Visualizar como condômino" />
                </div>
        </div>
        <span class="sair">
                        <img src="img/sair.png" alt="Sair" />
        </span>
        <div id="menu">
                <ul>
                        <li><a <?php if($pagina == "inicio"){echo $pagina_atual;} ?> href="home.php">Início</a></li>
                        <li><a <?php if($pagina == "avisos"){echo $pagina_atual;} ?> href="avisos.php">Avisos</a></li>
                        <li><a <?php if($pagina == "votacoes"){echo $pagina_atual;} ?> href="votacoes.php">Votações</a></li>
                        <li><a <?php if($pagina == "ocorrencias"){echo $pagina_atual;} ?> href="ocorrencias.php">Ocorrências</a></li>
                        <li><a <?php if($pagina == "mensagens"){echo $pagina_atual;} ?> href="mensagens.php">Mensagens</a></li>
                        <li><a <?php if($pagina == "documentos"){echo $pagina_atual;} ?> href="doc.php">Documentos</a></li>
                        <li><a <?php if($pagina == "boletos"){echo $pagina_atual;} ?> href="boletos.php">Boletos</a></li>
                </ul>
        </div>
        <div id="topoInfoVotacao">
                3 Votações encerradas não vistas
        </div>
</div>