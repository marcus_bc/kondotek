<?php
include_once '../config/conexao.php';

//Página atual para menu
$pagina = "avisos";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>

<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	
<meta http-equiv="Pragma" content="no-cache" />

<link rel="stylesheet" type="text/css" href="css/estilo.css" />

<title>Avisos - Kondotek</title>

</head>

<body>
	<div id="tudo">
		<?php include_once 'topo.php'; ?>
		<div id="conteudoSindico">
                    <?php
                        if(isset($_GET['cadastrar']) && $_GET['cadastrar'] == TRUE){
                    ?>
                    <div style="float: left; font-family: arial; margin-left: 10px; margin-top: 5px;">
                        <a href="home.php">Início</a>
                        » 
                        <a href="avisos.php">Avisos</a> 
                        » 
                        <span style="color:#666666;"><strong>Criar Aviso</strong></span>
                    </div>
                    <br /><br />
                    <div style="width: 90%; text-align: left; margin-top: 20px; margin-left: auto; margin-right: auto; font-family: arial; color: #666666; text-align: center;">
                        <form action="avisos_grava.php" method="post">
                            <?php if(isset($_SESSION['erro']['campo']['titulo'])){
                               echo "<p style='margin-left: 50px;'>Titulo: <input class='error' type='text' name='titulo' size='67' /></p>";
                               echo "<br />";
                               echo "<small class='error'>".$_SESSION['erro']['mensagem']['titulo']."</small>";
                           }else{?>
                               <p style="margin-left: 50px;">Titulo: <input type="text" name="titulo" size="67" /></p>
                           <?php } ?>
                           <br />
                           <?php if(isset($_SESSION['erro']['campo']['texto'])){
                               echo "<p style='margin-left: 50px;'><textarea class='error' name='texto' rows='17' cols='136'></textarea></p>";
                               echo "<br />";
                               echo "<small class='error'>".$_SESSION['erro']['mensagem']['texto']."</small>";
                           }else{?>
                               <p style="margin-left: 50px;"><textarea name="texto" rows="17" cols="136"></textarea></p>
                           <?php } ?>
                           <br />
                           <input type="submit" value="Cadastrar" class="button radius small" />
                           <br /><br />
                        </form>
                    </div>
		
                <?php
                    }elseif(isset($_GET['editar']) && $_GET['editar'] == TRUE){
                        $sqlEditar = "SELECT aviso, texto FROM cad_avisos WHERE id= '".$_GET['id']."'";
                        $consultaEditar = pg_query($con,$sqlEditar);
                        while ($linhaEditar = pg_fetch_assoc($consultaEditar)){
                            $titulo = $linhaEditar['aviso'];
                            $texto  = $linhaEditar['texto'];
                        }
                ?>
                <div style="float: left; font-family: arial; margin-left: 10px; margin-top: 5px;">
                        <a href="home.php">Início</a>
                        » 
                        <a href="avisos.php">Avisos</a> 
                        » 
                        <span style="color:#666666;"><strong>Editar Aviso</strong></span>
                    </div>
                    <br /><br />
                    <div style="width: 90%; text-align: left; margin-top: 20px; margin-left: auto; margin-right: auto; font-family: arial; color: #666666; text-align: center;">
                        <form action="avisos_grava.php" method="post">
                            <?php if(isset($_SESSION['erro']['campo']['titulo'])){
                               echo "<p style='margin-left: 50px;'>Titulo: <input class='error' type='text' name='titulo' size='67' /></p>";
                               echo "<br />";
                               echo "<small class='error'>".$_SESSION['erro']['mensagem']['titulo']."</small>";
                           }else{?>
                               <p style="margin-left: 50px;">Titulo: <input type="text" name="titulo" size="67" value='<?php echo $titulo; ?>' /></p>
                           <?php } ?>
                           <br />
                           <?php if(isset($_SESSION['erro']['campo']['texto'])){
                               echo "<p style='margin-left: 50px;'><textarea class='error' name='texto' rows='17' cols='136'></textarea></p>";
                               echo "<br />";
                               echo "<small class='error'>".$_SESSION['erro']['mensagem']['texto']."</small>";
                           }else{?>
                               <p style="margin-left: 50px;"><textarea name="texto" rows="17" cols="136"><?php echo $texto; ?></textarea></p>
                           <?php } ?>
                           <br />
                           <input type="hidden" name="editar" value="true" />
                           <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" />
                           <input type="submit" value="Cadastrar" class="button radius small" />
                           <br /><br />
                        </form>
                    </div>
                <?php }else{ ?>
                <div style="float: left; font-family: arial; margin-left: 10px; margin-top: 5px;">
                    <a href="home.php">Início</a>
                    » 
                    <span style="color:#666666;"><strong>Avisos</strong></span>
                </div>
                <br /><br />
                <div style="width: 70%; margin-top: 20px; margin-left: auto; margin-right: auto; font-family: arial;">
                    <table style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Título</th>
                                <th>Texto</th>
                                <th style='text-align:center; width: 150px;'>Ação</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $sql = "SELECT id, aviso, texto
                                        FROM cad_avisos 
                                        WHERE ativo = '0' 
                                        AND id_condominio = '".$_SESSION['id_condominio']."' 
                                        ORDER BY id DESC";
                                $consulta = pg_query($con,$sql);
                                while ($linha = pg_fetch_assoc($consulta)){
                                    echo "<tr>";
                                        echo "<td>";
                                            echo $linha['aviso'];
                                        echo "</td>";
                                        echo "<td>";
                                            echo $linha['texto'];
                                        echo "</td>";
                                        echo "<td style='text-align:center;'>";
                                            echo "<a href='avisos.php?editar=true&id=".$linha['id']."' class='button radius small secondary'>Editar</a>&nbsp;&nbsp;&nbsp;<a href='avisos_grava.php?excluir=true&id=".$linha['id']."' class='button radius small alert'>Excluir</a>";
                                        echo "</td>";
                                    echo "</tr>";
                                }
                            
                            ?>
                        </tbody>
                    </table>
                    <div id="clear"></div>
                </div>
                <?php } ?>
                </div>
            </div>

		<?php include_once 'rodape.php'; ?>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny.js"></script>
</body>
</html>