<?php
include_once '../config/conexao.php';
include_once '../config/funcoes.php';

//Página atual para menu
$pagina = "mensagens";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>

<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	
<meta http-equiv="Pragma" content="no-cache" />

<link rel="stylesheet" type="text/css" href="css/estilo.css" />

<title>Mensagens - Kondotek</title>

</head>

<body>
	<div id="tudo">
		<?php include_once 'topo.php'; ?>
		<div id="conteudoSindico">
                    <?php
                        if(isset($_GET['visualizar']) && $_GET['visualizar'] == TRUE){
                    ?>
                    <div style="float: left; font-family: arial; margin-left: 10px; margin-top: 5px;">
                        <a href="home.php">Início</a>
                        » 
                        <a href="mensagens.php">Mensagens</a> 
                        » 
                        <span style="color:#666666;"><strong>Visualizar Mensagem</strong></span>
                    </div>
                    <br /><br />
                    <div style="width: 90%; text-align: left; margin-top: 20px; margin-left: auto; margin-right: auto; font-family: arial; color: #666666; text-align: center; margin-bottom: 30px;">
                        <?php
                            $sql = "SELECT cm.id, cm.texto, to_char(cm.data_cadastro, 'DD/MM/YYYY') AS data_cadastro, cm.status, cu.nome
                                    FROM cad_mensagens AS cm
                                    left join cad_usuarios AS cu
                                    ON cu.id = cm.de
                                    WHERE cm.id_condominio = '".$_SESSION['id_condominio']."'
                                    AND cm.id = '".$_GET['id']."'";
                            $consulta = pg_query($con,$sql);
                            while ($linha = pg_fetch_assoc($consulta)){
                                $texto = $linha['texto'];
                                $condomino = $linha['nome'];
                                $data_cadastro = $linha['data_cadastro'];
                            }
                            echo "<div style='text-align:left; width:100%;'>
                                    <h3>Data da mensagem: </h3> ".$data_cadastro."
                                    <h3>Condômino: </h3> ".$condomino."
                                  </div>";
                            echo "<div style='width:100%;'>
                                    <h3>Mensagem</h3>
                                    <span style='float:right;'>
                                        <a href='#' class='button radius small'>Exibir mensagem anterior</a>
                                    </span>
                                    <br /><br />".$texto."
                                  </div>";
                            echo "<br />";
                            
                            echo "<div style='width:100%;'>
                                    <h3>Responder</h3>
                                    <br />
                                    <form action='mensagens_grava.php' method='post'>
                                        <textarea name='texto' style='width:100%' rows='25'></textarea>
                                        <br />
                                        <input class='button success radius small' type='submit' value='Enviar' />
                                    <form>
                                  </div>";

                            //Atualiza status para visualizado
                            if($_GET['status'] == 0){
                                $sqlStatus = "UPDATE cad_mensagens
                                                SET status = '1'
                                              WHERE id = '".$_GET['id']."'
                                              AND id_condominio = '".$_SESSION['id_condominio']."'";
                                $consultaStatus = pg_query($con,$sqlStatus);
                            }
                        ?>
                    </div>
		
                <?php
                    }elseif(isset($_GET['editar']) && $_GET['editar'] == TRUE){
                        $sqlEditar = "SELECT aviso, texto FROM cad_avisos WHERE id= '".$_GET['id']."'";
                        $consultaEditar = pg_query($con,$sqlEditar);
                        while ($linhaEditar = pg_fetch_assoc($consultaEditar)){
                            $titulo = $linhaEditar['aviso'];
                            $texto  = $linhaEditar['texto'];
                        }
                ?>
                <div style="float: left; font-family: arial; margin-left: 10px; margin-top: 5px;">
                        <a href="home.php">Início</a>
                        » 
                        <a href="avisos.php">Avisos</a> 
                        » 
                        <span style="color:#666666;"><strong>Editar Aviso</strong></span>
                    </div>
                    <br /><br />
                    <div style="width: 90%; text-align: left; margin-top: 20px; margin-left: auto; margin-right: auto; font-family: arial; color: #666666; text-align: center;">
                        <form action="avisos_grava.php" method="post">
                            <?php if(isset($_SESSION['erro']['campo']['titulo'])){
                               echo "<p style='margin-left: 50px;'>Titulo: <input class='error' type='text' name='titulo' size='67' /></p>";
                               echo "<br />";
                               echo "<small class='error'>".$_SESSION['erro']['mensagem']['titulo']."</small>";
                           }else{?>
                               <p style="margin-left: 50px;">Titulo: <input type="text" name="titulo" size="67" value='<?php echo $titulo; ?>' /></p>
                           <?php } ?>
                           <br />
                           <?php if(isset($_SESSION['erro']['campo']['texto'])){
                               echo "<p style='margin-left: 50px;'><textarea class='error' name='texto' rows='17' cols='136'></textarea></p>";
                               echo "<br />";
                               echo "<small class='error'>".$_SESSION['erro']['mensagem']['texto']."</small>";
                           }else{?>
                               <p style="margin-left: 50px;"><textarea name="texto" rows="17" cols="136"><?php echo $texto; ?></textarea></p>
                           <?php } ?>
                           <br />
                           <input type="hidden" name="editar" value="true" />
                           <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" />
                           <input type="submit" value="Cadastrar" class="button radius small" />
                           <br /><br />
                        </form>
                    </div>
                <?php }else{ ?>
                <div style="float: left; font-family: arial; margin-left: 10px; margin-top: 5px;">
                    <a href="home.php">Início</a>
                    » 
                    <span style="color:#666666;"><strong>Mensagens</strong></span>
                </div>
                <br /><br />
                <div style="width: 70%; margin-top: 20px; margin-left: auto; margin-right: auto; font-family: arial;">
                    <table style="width: 100%;">
                        <thead>
                            <tr>
                                <th style='text-align:center;'>Data da mensagem</th>
                                <th>Condômino</th>
                                <th>Mensagem</th>
                                <th style='text-align:center;'>Status</th>
                                <th style='text-align:center; width: 100px;'>Ação</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $sql = "SELECT cm.id, cm.texto, to_char(cm.data_cadastro, 'DD/MM/YYYY') AS data_cadastro, cm.status, cu.nome
                                        FROM cad_mensagens AS cm
                                        LEFT JOIN cad_usuarios AS cu
                                        ON cu.id = cm.de
                                        WHERE cm.para = '".$_SESSION['id_usuario']."' 
                                        AND cm.id_condominio = '".$_SESSION['id_condominio']."' 
                                        ORDER BY cm.id DESC";
                                $consulta = pg_query($con,$sql);
                                while ($linha = pg_fetch_assoc($consulta)){
                                    echo "<tr>";
                                        echo "<td style='text-align:center;'>";
                                            echo $linha['data_cadastro'];
                                        echo "</td>";
                                        echo "<td>";
                                            echo $linha['nome'];
                                        echo "</td>";
                                        echo "<td>";
                                            echo limita_caracteres($linha['texto'],150);
                                        echo "</td>";
                                        echo "<td style='text-align:center;'>";
                                            if($linha['status'] == 0){
                                                echo "<p style='color:#FF6600;'><strong>Não visualizada</strong></p>";
                                            }elseif($linha['status'] == 1){
                                                echo "Visualizada";
                                            }elseif($linha['status'] == 2){
                                                echo "<p style='color:green;'>Respondida</p>";
                                            }
                                        echo "</td>";
                                        echo "<td style='text-align:center;'>";
                                            echo "<a href='mensagens.php?visualizar=true&id=".$linha['id']."&status=".$linha['status']."' class='button radius small secondary'>Visualizar</a>";
                                        echo "</td>";
                                    echo "</tr>";
                                }
                            
                            ?>
                        </tbody>
                    </table>
                    <div id="clear"></div>
                </div>
                <?php } ?>
                </div>
            </div>

		<?php include_once 'rodape.php'; ?>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny.js"></script>
</body>
</html>