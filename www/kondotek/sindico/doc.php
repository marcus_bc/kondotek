<?php
include_once '../config/conexao.php';

//Página atual para menu
$pagina = "documentos";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>

<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	
<meta http-equiv="Pragma" content="no-cache" />

<link rel="stylesheet" type="text/css" href="css/estilo.css" />

<title>Documentos - Kondotek</title>

</head>

<body>
	<div id="tudo">
		<?php include_once 'topo.php'; ?>
		<div id="conteudoSindico">
                    <?php
                        if(isset($_GET['adicionar']) && $_GET['adicionar'] == TRUE){
                    ?>
                    <div style="float: left; font-family: arial; margin-left: 10px; margin-top: 5px;">
                        <a href="home.php">Início</a>
                        » 
                        <a href="doc.php">Documentos</a> 
                        » 
                        <span style="color:#666666;"><strong>Adicionar Documento</strong></span>
                    </div>
                    <br /><br />
                    <div style="width: 90%; text-align: left; margin-top: 20px; margin-left: auto; margin-right: auto; font-family: arial; color: #666666; text-align: center;">
                        <form action="doc_grava.php" method="post" enctype="multipart/form-data">
                            <?php if(isset($_SESSION['erro']['campo']['titulo'])){
                               echo "<p style='margin-left: 50px;'>Titulo: <input class='error' type='text' name='titulo' size='67' /></p>";
                               echo "<br />";
                               echo "<small class='error'>".$_SESSION['erro']['mensagem']['titulo']."</small>";
                           }else{?>
                               <p style="margin-left: 50px;">Titulo: <input type="text" name="titulo" size="67" /> <br /><br /></p>
                           <?php } ?>
                               <p><input type="radio" name="funcao" value="doc" /> Documento
                                   &nbsp;
                               <input type="radio" name="funcao" value="ata" /> Ata <br /><br /></p> 
                                <div class="panel" style="font-size: 14px;">
                                    <input type="file" name="doc" />
                                    <br /><br />
                                    <p>Só é permitido arquivos com extensão: doc, docx ou pdf</p> 
                                    <p>Tamanho máximo permitido: 2Mb</p>
                                </div>
                           <p><input type="submit" value="Adicionar" class="button radius small" /></p>
                        </form>
                    </div>
		
                <?php
                    }elseif(isset($_GET['download']) && $_GET['download'] == TRUE){
                        $sqlDownload = "SELECT documento, funcao FROM cad_doc WHERE id= '".$_GET['id']."' and id_condominio = '".$_SESSION['id_condominio']."' and ativo = '0'";
                        $consultaDownload = pg_query($con,$sqlDownload);
                        while ($linhaDownload = pg_fetch_assoc($consultaDownload)){
                            // Define o tempo máximo de execução em 0 para as conexões lentas
                            set_time_limit(0);
                            // Arqui você faz as validações e/ou pega os dados do banco de dados
                            $aquivoNome = $linhaDownload['documento']; // nome do arquivo que será enviado p/ download
                            if($linhaDownload['funcao'] == 0){
                                $arquivoLocal = '../uploads/'.$_SESSION['id_condominio'].'/ata/'.$aquivoNome; // caminho absoluto do arquivo
                            }else{
                                $arquivoLocal = '../uploads/'.$_SESSION['id_condominio'].'/'.$aquivoNome; // caminho absoluto do arquivo
                            }
                            
                            // Verifica se o arquivo não existe
                            if (!file_exists($arquivoLocal)) {
                                // Exiba uma mensagem de erro caso ele não exista
                                exit;
                            }
 
                            // Configuramos os headers que serão enviados para o browser
                            //header('Content-Description: File Transfer');
                            //header('Content-Type: application/pdf');
                            //header('Content-Type: application/msword');
                            //header('Content-type: octet/stream');
                            header('Content-Length: '.filesize($arquivoLocal));
                            header("Content-Disposition: attachment; filename=".basename($arquivoLocal));
                            //header('Content-Transfer-Encoding: binary');
                            //header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                            //header('Pragma: public');
                            //header('Expires: 0');

                            // Envia o arquivo para o cliente
                            readfile($arquivoLocal);

                            
                            //if($linhaDownload['funcao'] == 0){
                                //echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=../uploads/".$_SESSION['id_condominio']."/ata/".$linhaDownload['documento']."'>";
                               // die();  
                            //}else{
                               // echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=../uploads/".$_SESSION['id_condominio']."/".$linhaDownload['documento']."'>";
                               // die();
                           // }
                        }
                ?>
                <?php }else{ ?>
                <div style="float: left; font-family: arial; margin-left: 10px; margin-top: 5px;">
                    <a href="home.php">Início</a>
                    » 
                    <span style="color:#666666;"><strong>Documentos</strong></span>
                </div>
                <br /><br />
                <div style="width: 70%; margin-top: 20px; margin-left: auto; margin-right: auto; font-family: arial;">
                    <table style="width: 100%;">
                        <thead>
                            <tr>
                                <th>Título</th>
                                <th style='text-align:center;'>Tipo</th>
                                <th style='text-align:center; width: 200px;'>Ação</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $sql = "SELECT id, titulo, funcao
                                        FROM cad_doc 
                                        WHERE ativo = '0' 
                                        AND id_condominio = '".$_SESSION['id_condominio']."' 
                                        ORDER BY id DESC";
                                $consulta = pg_query($con,$sql);
                                while ($linha = pg_fetch_assoc($consulta)){
                                    echo "<tr>";
                                        echo "<td>";
                                            echo $linha['titulo'];
                                        echo "</td>";
                                        echo "<td style='text-align:center;'>";
                                            if($linha['funcao'] == 0){
                                                echo "Ata";
                                            }else{
                                                echo "Documento";
                                            }
                                        echo "</td>";
                                        echo "<td style='text-align:center;'>";
                                            echo "<a href='doc.php?download=true&id=".$linha['id']."' class='button radius small success'>Download</a>&nbsp;&nbsp;&nbsp;<a href='doc_grava.php?excluir=true&id=".$linha['id']."' class='button radius small alert'>Excluir</a>";
                                        echo "</td>";
                                    echo "</tr>";
                                }
                            
                            ?>
                        </tbody>
                    </table>
                    <div id="clear"></div>
                </div>
                <?php } ?>
                </div>
            </div>

		<?php include_once 'rodape.php'; ?>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny.js"></script>
</body>
</html>