<?php
include_once '../config/conexao.php';

if (!isset($_SESSION['logado']) || $_SESSION['logado'] == false){
    header('location:../index.php'); 
    die();
}

$pagina = '';

unset($erro);
unset($_SESSION['erro']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>

<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	
<meta http-equiv="Pragma" content="no-cache" />

<link rel="stylesheet" type="text/css" href="css/estilo.css" />

<title>Ata - Kondotek</title>

</head>

<body>
    <div id="tudo">
        <?php include_once 'topo.php'; ?>
        <div id="conteudoSindico" style="margin-bottom: 10px;">
            <div style="width: 400px; margin-left: auto; margin-right: auto; margin-top: 10%;">
					
<?php
        
	$titulo         = 	$_POST['titulo']; 
	$texto 		= 	$_POST['texto'];
        $doc 		= 	$_POST['doc'];
        
        if($titulo == '' && $texto == '' && $doc == 'NULL'){
            echo "<div class='alert-box alert' style='text-align:center; font-family:arial;'>
                        <p>Erro - Nenhum campo preenchido!</p>
                        <a href='javascript: history.back(-1);' class='button secondary radius small' style='margin:10px 0 5px;'>Ok</a>
                  </div>";
            die();
        }
        
        if($titulo == '' && $texto != ''){
            echo "<div class='alert-box alert' style='text-align:center; font-family:arial;'>
                        <p>Erro - Campo título não pode ser vazio!</p>
                        <a href='javascript: history.back(-1);' class='button secondary radius small' style='margin:10px 0 5px;'>Ok</a>
                  </div>";
            die();
        }
        
        if($titulo != '' && $texto == ''){
            echo "<div class='alert-box alert' style='text-align:center; font-family:arial;'>
                        <p>Erro - Campo texto não pode ser vazio!</p>
                        <a href='javascript: history.back(-1);' class='button secondary radius small' style='margin:10px 0 5px;'>Ok</a>
                  </div>";
            die();
        }
	
        if($titulo != ""){
            if(strlen($titulo) <= 2){ 
                    $_SESSION['erro']['campo']['titulo']        = TRUE;
                    $_SESSION['erro']['mensagem']['titulo']     = "Erro - O campo título deve possuir no minimo 3 caracteres.";
                    $erro = true; 
            }
        }
        
        if($texto != ""){
            if($texto == '' || strlen($texto) <= 2){ 
                    $_SESSION['erro']['campo']['texto']        = TRUE;
                    $_SESSION['erro']['mensagem']['texto']     = "Erro - O campo texto deve possuir no minimo 3 caracteres.";
                    $erro = true; 
            }
        }
        
	if(isset($erro) && $erro != ''){ 
		echo " 
			<script> 
				window.history.go(-1); 
			</script> 
		"; 
	}else{
            		
//-------------------------------CADASTRAR-------------------------------------- //
                
            $sql4 = "INSERT INTO cad_ata (ata, texto, data_cadastro, id_doc, ativo)
                        VALUES ('".$titulo."', '".$texto."', now(), ".$doc.", 0)";

            $consulta4 = pg_query($con,$sql4);

            if (!$consulta4) {
                echo "<div class='alert-box alert' style='text-align:center; font-family:arial;'>
                            <p>Erro ao cadastrar ata!</p>
                            <a href='javascript: history.back(-1);' class='button secondary radius small' style='margin:10px 0 5px;'>Ok</a>
                      </div>";
                die();
            }else{
                echo "<div class='alert-box success' style='text-align:center; font-family:arial;'>
                            <p>Ata cadastrada com sucesso!</p>
                            <a href='home.php' class='button radius small' style='margin:10px 0 5px;'>Ok</a>
                      </div>";
            }
	} 
	
 ?>
        </div>
    </div>
</body>
</html> 