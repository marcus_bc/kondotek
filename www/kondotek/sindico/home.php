<?php
include_once '../config/conexao.php';
unset($erro);
unset($_SESSION['erro']);
$pagina = "inicio";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>

<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	
<meta http-equiv="Pragma" content="no-cache" />

<link rel="stylesheet" type="text/css" href="css/estilo.css" />

<title>Home - Kondotek</title>

</head>

<body>
	<div id="tudo">
		<?php include_once 'topo.php'; ?>
		<div id="conteudoSindico" style="margin-bottom: 75px;">
                    <div id="quadroSindico">
                        <span class="btnPainelSindico">
                            <a href="avisos.php?cadastrar=true"><img src="img/btnCriarAvisos.jpg" alt="Criar Avisos" /></a>
                        </span>
                        <span class="btnPainelSindico">
                            <a href="ocorrencias.php" style="color: #FFFFFF;">
                                <span class="numPainelSindico">
                                    <?php 
                                        $sqlOcorrencias = "SELECT COUNT(id) AS total FROM cad_ocorrencias WHERE status = '0' AND id_condominio = '".$_SESSION['id_condominio']."'";
                                        $consultaOcorrencias = pg_query($con,$sqlOcorrencias);
                                        $linhaOcorrencias = pg_fetch_assoc($consultaOcorrencias);
                                        if($linhaOcorrencias['total'] > 0){
                                            echo $linhaOcorrencias['total'];
                                        }else{
                                            echo "0";
                                        }
                                    ?>
                                </span>
                                <img src="img/btnOcorrencias.png" alt="Ocorrências" />
                            </a>
                        </span>
                        <span class="btnPainelSindico">
                            <a href="mensagens.php" style="color: #FFFFFF;"><span class="numPainelSindico">3</span>
                            <img src="img/btnMensagens.png" alt="Mensagens" /></a>
                        </span>
                        <span class="btnPainelSindico">
                            <a href="votacao.php"><img src="img/btnCriarVotacao.jpg" alt="Criar Votação" /></a>
                        </span>
                        <span class="btnPainelSindico">
                            <a href="ata.php"><img src="img/btnCriarAta.jpg" alt="Criar Ata" /></a>
                        </span>
                        <span class="btnPainelSindico">
                            <a href="boletos.php"><img src="img/btnBoletos.jpg" alt="Boletos" /></a>
                        </span>
                        <span class="btnPainelSindico">
                            <a href="fotos.php"><img src="img/btnAdicionarFotos.jpg" alt="Adicionar Fotos" /></a>
                        </span>
                        <span class="btnPainelSindico">
                            <a href="doc.php?adicionar=true"><img src="img/btnAdicionarDocumentos.jpg" alt="Adicionar Documentos" /></a>
                        </span>
                        <span class="btnPainelSindico">
                            <a href="contas.php" style="color: #FFFFFF;"><span class="numPainelSindico">3</span>
                            <img src="img/btnContas.png" alt="Contas" /></a>
                        </span>
                    </div>
		</div>
        </div>
		<?php include_once 'rodape.php'; ?>
	
</body>
</html>