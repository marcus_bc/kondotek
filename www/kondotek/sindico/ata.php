<?php
include_once '../config/conexao.php';
$pagina = "";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>

<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	
<meta http-equiv="Pragma" content="no-cache" />

<link rel="stylesheet" type="text/css" href="css/estilo.css" />

<title>Criar Ata - Kondotek</title>

</head>

<body>
	<div id="tudo">
		<?php include_once 'topo.php'; ?>
		<div id="conteudoSindico">
                    <div style="float: left; font-family: arial; margin-left: 10px; margin-top: 5px;">
                        <a href="home.php">Início</a>
                        »
                        <span style="color:#666666;"><strong>Criar Ata</strong></span>
                    </div>
                    <br /><br />
                    <div style="width: 90%; text-align: left; margin-top: 20px; margin-left: auto; margin-right: auto; font-family: arial; color: #666666; text-align: center;">
                        <form action="ata_grava.php" method="post">
                               <div class="panel" style="margin-left: 50px;">
                                   Documento: 
                                   <select size="1" name="doc">
                                       <option selected="selected" value="NULL">-- Selecione --</option>
                                       <?php
                                            $sqlDoc = "SELECT id, documento
                                                          FROM cad_doc 
                                                          WHERE id_condominio = '".$_SESSION['id_condominio']."'
                                                          AND funcao = 0
                                                          ORDER BY id DESC
                                                          LIMIT 10";
                                            $consultaDoc = pg_query($con,$sqlDoc);
                                            while ($linhaDoc = pg_fetch_assoc($consultaDoc)){
                                                echo "<option value='".$linhaDoc['id']."'>".$linhaDoc['documento']."</option>";
                                            }
                                       ?>
                                   </select>
                                   <br /><br />
                                   <p style="font-size: 12px;">Para adicionar um documento, cadastre primeiramente no menu documentos.</p>
                               </div>
                             <?php if(isset($_SESSION['erro']['campo']['titulo'])){
                               echo "<p style='margin-left: 50px;'>Titulo: <input class='error' type='text' name='titulo' size='67' /></p>";
                               echo "<br />";
                               echo "<small class='error'>".$_SESSION['erro']['mensagem']['titulo']."</small>";
                           }else{?>
                               <p style="margin-left: 50px;">Titulo: <input type="text" name="titulo" size="67" /><br /><br /></p>
                           <?php } ?>
                           <?php if(isset($_SESSION['erro']['campo']['texto'])){
                               echo "<p style='margin-left: 50px;'><textarea class='error' name='texto' rows='17' cols='136'></textarea></p>";
                               echo "<br />";
                               echo "<small class='error'>".$_SESSION['erro']['mensagem']['texto']."</small>";
                           }else{?>
                               <p style="margin-left: 50px;"><textarea name="texto" rows="17" style="width:100%;"></textarea></p>
                           <?php } ?>
                           <p><br />
                           <input type="submit" value="Cadastrar" class="button radius small" />
                           <br /><br /></p>
                        </form>
                    </div>
                </div>
            </div>

		<?php include_once 'rodape.php'; ?>
<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="js/tiny.js"></script>
</body>
</html>