<?php
include_once '../config/conexao.php';

if (!isset($_SESSION['logado']) || $_SESSION['logado'] == false){
    header('location:../index.php'); 
    die();
}
	
unset($erro);
unset($_SESSION['erro']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>

<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	
<meta http-equiv="Pragma" content="no-cache" />

<link rel="stylesheet" type="text/css" href="css/estilo.css" />

<title>Avisos - Kondotek</title>

</head>

<body>
    <div id="tudo">
        <?php include_once 'topo.php'; ?>
        <div id="conteudoSindico" style="margin-bottom: 10px;">
            <div style="width: 400px; margin-left: auto; margin-right: auto; margin-top: 10%;">
					
<?php
        
//-------------------------------EXCLUIR-------------------------------------- //

	if(isset($_GET['excluir']) && $_GET['excluir'] == TRUE){
	
		$sql = "UPDATE cad_avisos
                            SET ativo 		 = '1',
                            data_exclusao        = now()
                         WHERE id = '".$_GET['id']."'";
                $consulta = pg_query($con,$sql);
                
                if (!$consulta) {
                    echo "<div class='alert-box alert' style='text-align:center; font-family:arial;'>
				<p>Erro ao excluir aviso!</p>
				<a href='javascript: history.back(-1);' class='button secondary radius small' style='margin:10px 0 5px;'>Ok</a>
			  </div>";
                    die();
                }else{
                    echo "<div class='alert-box success' style='text-align:center; font-family:arial;'>
				<p>Aviso excluido com sucesso!</p>
				<a href='avisos.php' class='button secondary radius small' style='margin:10px 0 5px;'>Ok</a>
			  </div>";
                    die();
                }
	
	}
	
	$titulo         = 	$_POST['titulo']; 
	$texto 		= 	$_POST['texto'];
	
	if($titulo == '' || strlen($titulo) <= 2){ 
		$_SESSION['erro']['campo']['titulo']        = TRUE;
		$_SESSION['erro']['mensagem']['titulo']     = "Erro - O campo título deve possuir no minimo 3 caracteres.";
		$erro = true; 
	} 

	if($texto == '' || strlen($texto) <= 2){ 
		$_SESSION['erro']['campo']['texto']        = TRUE;
		$_SESSION['erro']['mensagem']['texto']     = "Erro - O campo texto deve possuir no minimo 3 caracteres.";
		$erro = true; 
	}
        
	if(isset($erro) && $erro != ''){ 
		echo " 
			<script> 
				window.history.go(-1); 
			</script> 
		"; 
	}else{
            
//-------------------------------EDITAR-------------------------------------- //            
            
		if(isset($_POST['editar']) && $_POST['editar'] == TRUE){
		
			$sql2 = "UPDATE cad_avisos
                                            SET aviso              = '".$titulo."',
                                            texto                   = '".$texto."',
                                            data_alteracao          = now()    
                                WHERE id = '".$_POST['id']."'";
                        $consulta2 = pg_query($con,$sql2);
                        
                        if (!$consulta2) {
                            echo "<div class='alert-box alert' style='text-align:center; font-family:arial;'>
                                     <p>Erro ao editar aviso!</p>
                                     <a href='javascript: history.back(-1);' class='button secondary radius small' style='margin:10px 0 5px;'>Ok</a>
                                  </div>";
                            die();
                        }else{
                           echo "<div class='alert-box success' style='text-align:center; font-family:arial;'>
                                    <p>Aviso editado com sucesso!</p>
                                    <a href='avisos.php' class='button secondary radius small' style='margin:10px 0 5px;'>Ok</a>
                                </div>";
                           die();
                        }
	
		}
		
//-------------------------------CADASTRAR-------------------------------------- //
                
		$sql4 = "INSERT INTO cad_avisos (aviso, data_cadastro, id_condominio, texto)
                            VALUES ('".$titulo."', now(), '".$_SESSION['id_condominio']."', '".$texto."')";
		$consulta4 = pg_query($con,$sql4);
                
                if (!$consulta4) {
                    echo "<div class='alert-box alert' style='text-align:center; font-family:arial;'>
                                <p>Erro ao cadastrar aviso!</p>
                                <a href='javascript: history.back(-1);' class='button secondary radius small' style='margin:10px 0 5px;'>Ok</a>
                          </div>";
                    die();
                }else{
                    echo "<div class='alert-box success' style='text-align:center; font-family:arial;'>
				<p>Aviso cadastrado com sucesso!</p>
				<a href='home.php' class='button radius small' style='margin:10px 0 5px;'>Ok</a>
			  </div>";
                }
	} 
	
 ?>
        </div>
    </div>
</body>
</html> 