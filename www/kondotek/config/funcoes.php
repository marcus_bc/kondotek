<?php

//função que formata a data
function formata_data($data)
{
 //recebe o parâmetro e armazena em um array separado por -
 $data = explode('-', $data);
 //armazena na variavel data os valores do vetor data e concatena /
 $data = $data[2].'/'.$data[1].'/'.$data[0];
  
 //retorna a string da ordem correta, formatada
 return $data;
}

//função que limita caracteres, tira TAGS e adiciona ...
function limita_caracteres($texto, $qtde) //texto que sera limitado e quantidade maxima permitida de caracteres
{
    if(strlen($texto) > $qtde){
        $limite_caracteres = strip_tags(substr($texto,0,$qtde));
        $posicao = strrpos($limite_caracteres, ' ');
        return strip_tags(substr($limite_caracteres,0,$posicao))."...";
    }else{
        return $texto;
    }
}
 

function formata_data_extenso($strDate){

	$arrDaysOfWeek = array('Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado');
	
	$arrMonthsOfYear = array(1 => 'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');

	$intDayOfWeek = date('w',strtotime($strDate));

	$intDayOfMonth = date('d',strtotime($strDate));

	$intMonthOfYear = date('n',strtotime($strDate));

	$intYear = date('Y',strtotime($strDate));

	return $arrDaysOfWeek[$intDayOfWeek] . ', ' . $intDayOfMonth . ' de ' . $arrMonthsOfYear[$intMonthOfYear] . ' de ' . $intYear;

}
?>
