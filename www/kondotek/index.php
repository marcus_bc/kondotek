<?php
    session_start();
    if (!isset($_SESSION['logado']) || $_SESSION['logado'] == "")
        $_SESSION['logado'] = false;
    
    $_SESSION['hoje'] = date("d/m/Y");
    $_SESSION['pagina']= "";
    
	
    if ($_SESSION['logado'] == false){
        echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=login.php'>";
        die();
    }
    
    if ($_SESSION['logado'] == true){
    	if($_SESSION['nivel'] == '1'){ //Redireciona para página inicial de condomino
            echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=condomino/home.php'>";
            die();
        }elseif($_SESSION['nivel'] == '2'){//Redireciona para página inicial de sindico
            echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=sindico/home.php'>";
            die();
        }elseif($_SESSION['nivel'] == '3'){//Redireciona para página inicial de administrador
            echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=admin/home.php'>";
            die();
        }else{//Redireciona para página de erro, pois nivel não existe
            echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=404.php'>";
            die();
        }
    }
    
?>