<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>

<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	
<meta http-equiv="Pragma" content="no-cache" />

<link rel="stylesheet" type="text/css" href="css/estilo.css" />

<title>Home - Kondotek</title>

</head>

<body>
	<div id="tudo">
		<div id="topo">
			<div id="logo"><img src="img/logo.png" alt="logo" /></div>
			<div id="topoInfoNome">
				<span class="titulo">Pedro Aguiar</span>
				<br />
				<span class="subtitulo">Último Acesso - 12/12/2012</span>
				<div id="visualizarcomo">
					<img src="img/btnvisualizarcomo.png" alt="Visualizar como condômino" />
				</div>
			</div>
			<span class="sair">
					<img src="img/sair.png" alt="Sair" />
			</span>
			<div id="menu">
				<ul>
					<li><a class="ativo" href="index.html">Início</a></li>
					<li><a href="#">Avisos</a></li>
					<li><a href="#">Votações</a></li>
					<li><a href="#">Ocorrências</a></li>
					<li><a href="#">Mensagens</a></li>
					<li><a href="#">Documentos</a></li>
					<li><a href="#">Boletos</a></li>
				</ul>
			</div>
			<div id="topoInfoVotacao">
				3 Votações encerradas não vistas
			</div>
		</div>
		<div id="conteudoSindico">
                    <div id="quadroSindico">
                        <span class="btnPainelSindico">
                            <img src="img/btnCriarAvisos.jpg" alt="Criar Avisos" />
                        </span>
                        <span class="btnPainelSindico">
                            <span class="numPainelSindico">3</span>
                            <img src="img/btnOcorrencias.png" alt="Ocorrências" />
                        </span>
                        <span class="btnPainelSindico">
                            <span class="numPainelSindico">3</span>
                            <img src="img/btnMensagens.png" alt="Mensagens" />
                        </span>
                        <span class="btnPainelSindico">
                            <img src="img/btnCriarVotacao.jpg" alt="Criar Votação" />
                        </span>
                        <span class="btnPainelSindico">
                            <img src="img/btnCriarAta.jpg" alt="Criar Ata" />
                        </span>
                        <span class="btnPainelSindico">
                            <img src="img/btnBoletos.jpg" alt="Boletos" />
                        </span>
                        <span class="btnPainelSindico">
                            <img src="img/btnAdicionarFotos.jpg" alt="Adicionar Fotos" />
                        </span>
                        <span class="btnPainelSindico">
                            <img src="img/btnAdicionarDocumentos.jpg" alt="Adicionar Documentos" />
                        </span>
                        <span class="btnPainelSindico">
                            <span class="numPainelSindico">3</span>
                            <img src="img/btnContas.png" alt="Contas" />
                        </span>
                    </div>
		</div>
		<div id="rodapeSindico">
			<div id="contato">
				<p style="color:#FF6600;">www.i7informatica.com.br/kondotek</p>
				<p style="color:#666666;">contato@i7informatica.com.br</p>
				<p style="color:#666666;">Copyright i7 Soluções em Informática</p>
			</div>
			<div id="btnRodape">
				<p><img src="img/btnSugestoes.png" alt="Sugestões" /></p>
				<p><img src="img/btnManual.png" alt="Manual" /></p>
			</div>
		</div>
	</div>
</body>
</html>