<?php
	include_once 'config/conexao.php';
        
        $usuario        = isset($_POST["usuario"])      ? addslashes($_POST["usuario"])     : "0";
        $senha          = isset($_POST["senha"])        ? addslashes($_POST["senha"])       : "0";
        $condominio     = isset($_POST["condominio"])   ? addslashes($_POST["condominio"])  : "0";
        
        $senha_crip = md5($senha);

	$sql = "SELECT cu.id, cu.nome, ca.qtde_acessos, cu.id_condominio, cu.id_nivel, to_char(ca.data_acesso, 'DD/MM/YYYY') AS data_acesso
                FROM cad_usuarios as cu
                
                LEFT JOIN cad_acessos AS ca
                    ON ca.id_usuario = cu.id
                    
                WHERE cu.usuario = '$usuario' AND cu.senha = '$senha_crip' AND cu.id_condominio = '$condominio'  AND cu.ativo = '0'";
        
	$consulta = pg_query($con,$sql);	
	if (pg_num_rows($consulta) >= 1){
		while ($linha = pg_fetch_assoc($consulta)){
			$_SESSION['nome'] = $linha['nome'];
			$_SESSION['id_usuario'] = $linha['id'];
                        $_SESSION['id_condominio'] = $linha['id_condominio'];
                        $_SESSION['ultimo_acesso'] = $linha['data_acesso'];
                        $_SESSION['nivel'] = $linha['id_nivel'];
			$qtdeAcessos = $linha['qtde_acessos'] + 1;
		}
                
                //verificar o navegador do usuario logado 
                $useragent = $_SERVER['HTTP_USER_AGENT'];
                   if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
                     $_SESSION['browser_version']=$matched[1];
                     $_SESSION['browser'] = 'IE';
                   } elseif (preg_match( '|Opera/([0-9].[0-9]{1,2})|',$useragent,$matched)) {
                     $_SESSION['browser_version']=$matched[1];
                     $_SESSION['browser'] = 'Opera';
                   } elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
                     $_SESSION['browser_version']=$matched[1];
                     $_SESSION['browser'] = 'Firefox';
                   } elseif(preg_match('|Chrome/([0-9\.]+)|',$useragent,$matched)) {
                     $_SESSION['browser_version']=$matched[1];
                     $_SESSION['browser'] = 'Chrome';
                   } elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
                     $_SESSION['browser_version']=$matched[1];
                     $_SESSION['browser'] = 'Safari';
                   } else {
                     // Navegador não reconhecido!
                     $_SESSION['browser_version'] = 0;
                     $_SESSION['browser']= 'outro';
                   }
                   
                   $sql3 = "INSERT INTO cad_acessos (data_acesso, qtde_acessos, navegador, id_usuario)
                                VALUES (now(), '".$qtdeAcessos."', '".$_SESSION['browser']."', '".$_SESSION['id_usuario']."') returning id";
                   $consulta3 = pg_query($con,$sql3);
                   if (is_resource($consulta3)) {
                   $obj = pg_fetch_object($consulta3);
                   $_SESSION['id_acessos'] = $obj->id;
                   }

                $_SESSION['logado'] = true;
                
                
                if($_SESSION['nivel'] == '1'){ //Redireciona para página inicial de condomino
                    echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=condomino/home.php'>";
                    die();
                }elseif($_SESSION['nivel'] == '2'){//Redireciona para página inicial de sindico
                    echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=sindico/home.php'>";
                    die();
                }elseif($_SESSION['nivel'] == '3'){//Redireciona para página inicial de administrador
                    echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=admin/home.php'>";
                    die();
                }else{//Redireciona para página de erro, pois nivel não existe
                    echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=404.php'>";
                    die();
                }
                
                
	}else{
		//$sql4 = "INSERT INTO cad_acessos (acessou, usuario, senha, data_acesso)
                    //VALUES ('N', '".$usuario."', '".$senha."', now())";
		//$consulta4 = pg_query($con,$sql4);
		echo "<meta HTTP-EQUIV='Refresh' CONTENT='0;URL=login.php?erro=1'>";
		die();
	}
?>