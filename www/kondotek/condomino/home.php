<?php
include_once '../config/conexao.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>

<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	
<meta http-equiv="Pragma" content="no-cache" />

<link rel="stylesheet" type="text/css" href="css/estilo.css" />

<link rel="stylesheet" type="text/css" href="css/global.css" />

<title>Home - Kondotek</title>
  
</head>

<body>
	<div id="tudo">
		<?php include_once 'topo.php'; ?>
		<div id="conteudo">
			<h2>Últimos Avisos</h2>
			<div id="quadro">
				<div id="anotacao">
                                    <?php
                                        $sqlAvisos = "SELECT ca.id, ca.texto, cav.data_visualizacao
                                                        FROM cad_avisos AS ca 
                                                        
                                                        LEFT JOIN cad_avisos_visualizados AS cav
                                                            ON cav.id_aviso = ca.id

                                                        WHERE ca.ativo = '0' LIMIT 6";
                                         $consultaAvisos = pg_query($con,$sqlAvisos); 
                                         while ($linhaAvisos = pg_fetch_assoc($consultaAvisos)){
                                            if($linhaAvisos['data_visualizacao'] != ''){
                                                echo "<div class='anotacaolaranja'>";
                                                    echo "<div class='box'>";
							echo "<a href='avisos.php?id=".$linhaAvisos['id']."'>".$linhaAvisos['texto']."</a>";
                                                    echo "</div>";
                                                echo "</div>";
                                            }else{
                                                echo "<div class='anotacaoverde'>";
                                                    echo "<div class='box'>";
							echo "<a href='avisos.php?id=".$linhaAvisos['id']."'>".$linhaAvisos['texto']."</a>";
                                                    echo "</div>";
                                                echo "</div>";
                                            }
                                         }
                                    ?>
				</div>
			</div>
		</div>
		<div id="carrossel">
			<div id="container">
				<div id="example">
					<div id="slides">
						<div class="slides_container">
                                                    <?php
                                                        $sqlSlides = "SELECT id, imagem, diretorio, titulo 
                                                                        FROM cad_img_condominio 
                                                                        WHERE ativo = '0' 
                                                                            AND funcao = '0' 
                                                                            AND id_condominio = '".$_SESSION['id_condominio']."' 
                                                                            ORDER BY id DESC";
                                                        $consultaSlides = pg_query($con,$sqlSlides);
                                                        while ($linhaSlides = pg_fetch_assoc($consultaSlides)){
                                                            echo "<div class='slide'>";
								echo "<a href='#' target='_blank'><img src='".$linhaSlides['diretorio']."/".$linhaSlides['imagem']."' width='570' height='270' alt='Slide'></a>";
								echo "<div class='caption'>";
									echo "<p>".$linhaSlides['titulo']."</p>";
								echo "</div>";
                                                            echo "</div>";
                                                        }
                                                    ?>
						</div>
						<a href="#" class="prev"><img src="img/arrow-prev.png" width="24" height="43" alt="Arrow Prev"></a>
						<a href="#" class="next"><img src="img/arrow-next.png" width="24" height="43" alt="Arrow Next"></a>
					</div>
					<img src="img/example-frame.png" width="739" height="341" alt="Example Frame" id="frame">
				</div>
			</div>
		</div>
		<div id="votacao">
			<div id="votacaoinfo">
				<p style="color:#FF6600; font-size:24px; font-weight:bold;">Votação</p>
				<br />
				<p style="color:#666666; font-size:16px; font-weight:bold;">Manutenção da quadra de esportes: trocar o piso de madeira por tinta emborrachada?</p>
				<br />
				<span style="margin-right:380px;"><img src="img/seta-esquerda-desat.png" alt="Voltar" /></span>
				<span style="text-align:right;"><img src="img/seta-direita.png" alt="Avançar" /></span>
				<p><input type="button" value="SIM" class="votacao" /><input type="button" value="NÃO" class="votacao" /></p>
				<br /><br />
				<p style="color:#FF6600; font-size:14px; font-weight:bold;">Clique aqui para baixar ata</p>
			</div>
		</div>
		<?php include_once 'rodape.php'; ?>
	</div>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/slides.min.jquery.js"></script>
<script type="text/javascript" src="js/slides_config.js"></script>
</body>
</html>