<?php
include_once 'config/conexao.php';

if(isset($_SESSION['logado']) && $_SESSION['logado'] == true){
    header('location:index.php'); 
    die();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="css/estilo.css" />

<title>Login - Kondotek</title>
   
</head>

<body>
	<div id="tudo">
		<div id="topo">
			<div id="logo"><img src="img/logo.png" alt="logo" /></div>
		</div>
                <?php
                    if(isset($_GET['erro']) && $_GET['erro'] == '1'){
                        echo "<div class='erroLogin' style='background-color:red;'>";
                        echo "<p>Erro de login, tente novamente!</p>";
                        echo "</div>";
                    }else{
                        echo "<div class='erroLogin'>";
                        echo "</div>";
                    }
                ?>
		<div id="login">
                    <div id="camposLogin">
			<form action="valida.php" method="post" id="form" >
				<p>Condominio:</p>
				<p><select size="1" name="condominio">
                                        <option selected="selected" value="0">-- Selecione --</option>
                                        <?php
                                            $sqlCondominio = "select id, nome from cad_condominio";
                                            $consultaCondominio = pg_query($con,$sqlCondominio); 
                                            while ($linhaCondominio = pg_fetch_assoc($consultaCondominio)){
                                                echo "<option value='".$linhaCondominio['id']."'>".$linhaCondominio['nome']."</option>";
                                            }
                                        ?>
				</select></p>
                                
				<p style="margin-top: 20px;">Usuário: <input type="text" name="usuario" class="input" /></p>
				
				<p style="margin-top: 20px;">Senha: &nbsp;&nbsp;&nbsp;<input type="password" name="senha" class="input" id="senha" /></p>
				
				<p style="margin-top: 20px;"><a href="#" onclick="document.getElementById('form').submit();"><img src="img/btnEntrar.png" alt="Entrar" /></a></p>
                                
                                <p style="font-size: 12px;"><a href="#">Esqueceu sua senha? Clique Aqui!</a></p>
			</form>
                    </div>
		</div>
	</div>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcoes.js"></script>
</body>
</html>