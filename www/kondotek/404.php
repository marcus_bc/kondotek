<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>

<meta http-equiv="content-type" content="text/html; charset=UTF-8" />

<link rel="stylesheet" type="text/css" href="css/estilo.css" />

<title>Login - Kondotek</title>
   
</head>

<body>
    <div id="tudo">
        <div id="topo">
                <div id="logo"><img src="img/logo.png" alt="logo" /></div>
        </div>
        <div style="width:100%; text-align: center; margin-top: 150px; font-family: arial; color: #FF6600;">
            <h1>Página não encontrada!</h1>
            <br />
            <a style="text-decoration: none;" href="index.php"><img src="img/btnVoltar.png" alt="" /></a>
        </div>
    </div>
</body>
</html>