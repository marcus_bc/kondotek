--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.10
-- Dumped by pg_dump version 9.3.10
-- Started on 2015-10-16 18:25:55 BRT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 2108 (class 1262 OID 16384)
-- Name: kondotek; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE kondotek WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'pt_BR.UTF-8' LC_CTYPE = 'pt_BR.UTF-8';


ALTER DATABASE kondotek OWNER TO postgres;

\connect kondotek

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 187 (class 3079 OID 11791)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2111 (class 0 OID 0)
-- Dependencies: 187
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 176 (class 1259 OID 16465)
-- Name: avisos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE avisos (
    id integer NOT NULL,
    titulo character varying(100) NOT NULL,
    texto text,
    id_usuario integer NOT NULL,
    id_condominio integer NOT NULL,
    data_cadastro timestamp without time zone DEFAULT now() NOT NULL,
    ativo boolean DEFAULT true
);


ALTER TABLE public.avisos OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 16463)
-- Name: avisos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE avisos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.avisos_id_seq OWNER TO postgres;

--
-- TOC entry 2112 (class 0 OID 0)
-- Dependencies: 175
-- Name: avisos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE avisos_id_seq OWNED BY avisos.id;


--
-- TOC entry 180 (class 1259 OID 16552)
-- Name: avisos_usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE avisos_usuario (
    id integer NOT NULL,
    visualizado boolean DEFAULT false NOT NULL,
    id_usuario integer NOT NULL,
    id_aviso integer NOT NULL,
    id_condominio integer NOT NULL,
    data_cadastro timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.avisos_usuario OWNER TO postgres;

--
-- TOC entry 2113 (class 0 OID 0)
-- Dependencies: 180
-- Name: TABLE avisos_usuario; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE avisos_usuario IS 'Tabela de vinculo entre a tabela de avisos e a de usuario';


--
-- TOC entry 179 (class 1259 OID 16550)
-- Name: avisos_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE avisos_usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.avisos_usuario_id_seq OWNER TO postgres;

--
-- TOC entry 2114 (class 0 OID 0)
-- Dependencies: 179
-- Name: avisos_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE avisos_usuario_id_seq OWNED BY avisos_usuario.id;


--
-- TOC entry 170 (class 1259 OID 16397)
-- Name: ci_sessions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ci_sessions (
    id character varying(40) DEFAULT '0'::character varying NOT NULL,
    ip_address character varying(45) DEFAULT '0'::character varying NOT NULL,
    "timestamp" bigint DEFAULT 0 NOT NULL,
    data text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.ci_sessions OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 16412)
-- Name: condominio; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE condominio (
    id integer NOT NULL,
    nome character varying(150) NOT NULL,
    telefone character varying(35),
    email character varying(100),
    endereco character varying(200),
    ativo smallint DEFAULT 1 NOT NULL,
    data_cadastro timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.condominio OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 16410)
-- Name: condominio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE condominio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.condominio_id_seq OWNER TO postgres;

--
-- TOC entry 2115 (class 0 OID 0)
-- Dependencies: 171
-- Name: condominio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE condominio_id_seq OWNED BY condominio.id;


--
-- TOC entry 186 (class 1259 OID 16646)
-- Name: mensagens; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE mensagens (
    id integer NOT NULL,
    titulo character varying(100) NOT NULL,
    texto text,
    id_usuario integer NOT NULL,
    id_condominio integer NOT NULL,
    data_cadastro timestamp without time zone DEFAULT now() NOT NULL,
    ativo boolean DEFAULT true
);


ALTER TABLE public.mensagens OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 16644)
-- Name: mensagens_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE mensagens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mensagens_id_seq OWNER TO postgres;

--
-- TOC entry 2116 (class 0 OID 0)
-- Dependencies: 185
-- Name: mensagens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE mensagens_id_seq OWNED BY mensagens.id;


--
-- TOC entry 182 (class 1259 OID 16586)
-- Name: ocorrencias; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ocorrencias (
    id integer NOT NULL,
    texto text NOT NULL,
    id_usuario integer NOT NULL,
    id_condominio integer NOT NULL,
    data_cadastro timestamp without time zone DEFAULT now() NOT NULL,
    ativo boolean DEFAULT true
);


ALTER TABLE public.ocorrencias OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 16584)
-- Name: ocorrencias_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ocorrencias_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ocorrencias_id_seq OWNER TO postgres;

--
-- TOC entry 2117 (class 0 OID 0)
-- Dependencies: 181
-- Name: ocorrencias_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ocorrencias_id_seq OWNED BY ocorrencias.id;


--
-- TOC entry 184 (class 1259 OID 16619)
-- Name: ocorrencias_usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ocorrencias_usuario (
    id integer NOT NULL,
    visualizado boolean DEFAULT false NOT NULL,
    id_usuario integer NOT NULL,
    id_ocorrencia integer NOT NULL,
    id_condominio integer NOT NULL,
    data_cadastro timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.ocorrencias_usuario OWNER TO postgres;

--
-- TOC entry 2118 (class 0 OID 0)
-- Dependencies: 184
-- Name: TABLE ocorrencias_usuario; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE ocorrencias_usuario IS 'Tabela de vinculo entre a tabela de ocorrencias e a de usuario';


--
-- TOC entry 183 (class 1259 OID 16617)
-- Name: ocorrencias_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ocorrencias_usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ocorrencias_usuario_id_seq OWNER TO postgres;

--
-- TOC entry 2119 (class 0 OID 0)
-- Dependencies: 183
-- Name: ocorrencias_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ocorrencias_usuario_id_seq OWNED BY ocorrencias_usuario.id;


--
-- TOC entry 174 (class 1259 OID 16422)
-- Name: usuarios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuarios (
    id integer NOT NULL,
    nome character varying(150) NOT NULL,
    cpf character varying(100),
    telefone character varying(45),
    endereco character varying(200),
    email character varying(150) NOT NULL,
    nivel_acesso smallint DEFAULT 0 NOT NULL,
    senha character varying(255) NOT NULL,
    id_condominio integer NOT NULL,
    ativo smallint DEFAULT 1 NOT NULL,
    data_cadastro timestamp without time zone DEFAULT now(),
    cidade character varying(100),
    estado character varying(100),
    pais character varying(100)
);


ALTER TABLE public.usuarios OWNER TO postgres;

--
-- TOC entry 2120 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN usuarios.nivel_acesso; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN usuarios.nivel_acesso IS '0 - Condomino
1 - Sindico';


--
-- TOC entry 173 (class 1259 OID 16420)
-- Name: usuarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_id_seq OWNER TO postgres;

--
-- TOC entry 2121 (class 0 OID 0)
-- Dependencies: 173
-- Name: usuarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE usuarios_id_seq OWNED BY usuarios.id;


--
-- TOC entry 178 (class 1259 OID 16494)
-- Name: votacao; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE votacao (
    id integer NOT NULL,
    titulo text NOT NULL,
    id_usuario integer NOT NULL,
    id_condominio integer NOT NULL,
    data_cadastro timestamp without time zone DEFAULT now() NOT NULL,
    ativo boolean DEFAULT true NOT NULL,
    finalizada boolean DEFAULT false NOT NULL
);


ALTER TABLE public.votacao OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 16492)
-- Name: votacao_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE votacao_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.votacao_id_seq OWNER TO postgres;

--
-- TOC entry 2122 (class 0 OID 0)
-- Dependencies: 177
-- Name: votacao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE votacao_id_seq OWNED BY votacao.id;


--
-- TOC entry 1926 (class 2604 OID 16468)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avisos ALTER COLUMN id SET DEFAULT nextval('avisos_id_seq'::regclass);


--
-- TOC entry 1933 (class 2604 OID 16555)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avisos_usuario ALTER COLUMN id SET DEFAULT nextval('avisos_usuario_id_seq'::regclass);


--
-- TOC entry 1919 (class 2604 OID 16415)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY condominio ALTER COLUMN id SET DEFAULT nextval('condominio_id_seq'::regclass);


--
-- TOC entry 1942 (class 2604 OID 16649)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY mensagens ALTER COLUMN id SET DEFAULT nextval('mensagens_id_seq'::regclass);


--
-- TOC entry 1936 (class 2604 OID 16589)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ocorrencias ALTER COLUMN id SET DEFAULT nextval('ocorrencias_id_seq'::regclass);


--
-- TOC entry 1939 (class 2604 OID 16622)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ocorrencias_usuario ALTER COLUMN id SET DEFAULT nextval('ocorrencias_usuario_id_seq'::regclass);


--
-- TOC entry 1922 (class 2604 OID 16425)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuarios ALTER COLUMN id SET DEFAULT nextval('usuarios_id_seq'::regclass);


--
-- TOC entry 1929 (class 2604 OID 16497)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY votacao ALTER COLUMN id SET DEFAULT nextval('votacao_id_seq'::regclass);


--
-- TOC entry 2093 (class 0 OID 16465)
-- Dependencies: 176
-- Data for Name: avisos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (10, 'AVISO TESTE 1', 'TEXTO AVISO TESTE 1', 1, 1, '2015-09-30 21:28:19.68586', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (11, 'AVISO TESTE 2', 'TEXTO AVISO TESTE 2', 1, 1, '2015-09-30 21:28:31.494604', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (12, 'AVISO TESTE 3', 'TEXTO AVISO TESTE 3', 1, 1, '2015-09-30 21:28:43.72762', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (13, 'AVISO TESTE 4', 'TEXTO AVISO TESTE 4', 1, 1, '2015-09-30 21:28:53.728435', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (14, 'AVISO TESTE 5', 'TEXTO AVISO TESTE 5', 1, 1, '2015-09-30 21:29:06.707643', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (15, 'AVISO TESTE 6', 'TEXTO AVISO TESTE 6', 1, 1, '2015-09-30 21:29:15.691813', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (16, 'RECEPÇÃO', 'KLJFKLSDJFKSJDFLKSJLFKSJF
DFKJSLFSJLDFKJSLKFJSFJSDF
DKFJSDLKFJLSDKJFSKJFLSDKFJSDF
DKLFJSDLKFJSLKFJLSKFKSJFKSJFSD
KSLJFLKSJFKLSJDFLKSJDFKSJDFSDF
SDFKSJFLKSJLFKSJDLFJSLFKJSDLFKJSLDFKSDJFLSKDJFLKSFJSF
SKDJFSLKDJFLSKDJFLSKDJFLKSDJLFKJSDKJFLSDKJFSKDJFSKLDJFSKDFJSLKF
LÇSDKFÇLKSÇFLKSÇDLFKSDLKFÇSDLKFÇSLKFÇLSKFSLDKFSLFÇSLFKÇSLDKF', 1, 1, '2015-10-01 23:37:18.616554', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (17, 'TESTE', 'TESTE', 1, 1, '2015-10-15 21:24:08.338544', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (18, 'ASDASDASD', 'ASDASDASD', 1, 1, '2015-10-15 21:40:38.67106', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (19, '1111', '22222', 1, 1, '2015-10-15 21:43:53.328108', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (20, '222', '3333', 1, 1, '2015-10-15 21:46:38.166092', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (21, 'SDFDSF', 'SDFSDFSD', 1, 1, '2015-10-15 21:47:44.162281', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (22, '5555', 'ASDADSASD', 1, 1, '2015-10-15 21:49:47.572551', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (23, '7777', 'FDSFSDFSDFSDF', 1, 1, '2015-10-15 21:50:52.68545', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (24, '888', 'ASDASDASDAS', 1, 1, '2015-10-15 21:51:23.454389', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (25, 'SDFSDFSDF', 'SDFSDFSDFDSF', 1, 1, '2015-10-15 21:53:09.460736', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (26, 'DASDASD', 'ASDASDASD', 1, 1, '2015-10-15 21:54:18.428465', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (27, '9999', 'FSDFSDFSDF', 1, 1, '2015-10-15 21:58:09.841809', true);
INSERT INTO avisos (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (28, 'JHSAHDJHH', 'JHSKJHDKJASDKJAS', 1, 1, '2015-10-15 23:47:06.531844', true);


--
-- TOC entry 2123 (class 0 OID 0)
-- Dependencies: 175
-- Name: avisos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('avisos_id_seq', 28, true);


--
-- TOC entry 2097 (class 0 OID 16552)
-- Dependencies: 180
-- Data for Name: avisos_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (1, false, 1, 10, 1, '2015-09-30 21:28:19.68586');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (4, true, 1, 13, 1, '2015-09-30 21:28:53.728435');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (3, true, 1, 12, 1, '2015-09-30 21:28:43.72762');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (2, true, 1, 11, 1, '2015-09-30 21:28:31.494604');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (7, true, 1, 16, 1, '2015-10-01 23:37:18.616554');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (6, true, 1, 15, 1, '2015-09-30 21:29:15.691813');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (5, true, 1, 14, 1, '2015-09-30 21:29:06.707643');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (8, false, 1, 17, 1, '2015-10-15 21:24:08.338544');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (9, false, 1, 18, 1, '2015-10-15 21:40:38.67106');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (10, false, 1, 19, 1, '2015-10-15 21:43:53.328108');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (11, false, 1, 20, 1, '2015-10-15 21:46:38.166092');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (12, false, 1, 21, 1, '2015-10-15 21:47:44.162281');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (13, false, 1, 22, 1, '2015-10-15 21:49:47.572551');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (14, false, 1, 23, 1, '2015-10-15 21:50:52.68545');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (15, false, 1, 24, 1, '2015-10-15 21:51:23.454389');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (16, false, 1, 25, 1, '2015-10-15 21:53:09.460736');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (17, false, 1, 26, 1, '2015-10-15 21:54:18.428465');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (18, false, 1, 27, 1, '2015-10-15 21:58:09.841809');
INSERT INTO avisos_usuario (id, visualizado, id_usuario, id_aviso, id_condominio, data_cadastro) VALUES (19, false, 1, 28, 1, '2015-10-15 23:47:06.531844');


--
-- TOC entry 2124 (class 0 OID 0)
-- Dependencies: 179
-- Name: avisos_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('avisos_usuario_id_seq', 19, true);


--
-- TOC entry 2087 (class 0 OID 16397)
-- Dependencies: 170
-- Data for Name: ci_sessions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('abed27a8b9250eb345c099904a776fbdad55d327', '127.0.0.1', 1439846770, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDM5ODQ2NzcwOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('53bb679067d4c7bee2457105a1e7c87fac0ae1b3', '127.0.0.1', 1439847308, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDM5ODQ3MzA4Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('766299d95921aedb28ebc38c9bd357ac26e3c656', '127.0.0.1', 1440972184, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTcyMTg0Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('18048cfedd4076951019be962dca7776e898290d', '127.0.0.1', 1440973389, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTczMTc3Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('4028859d2bf5025fab23d78978879202aca7ec2c', '127.0.0.1', 1440973559, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTczNTU5Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('9608159c44aec49b6e980383740f2d2a967abbbc', '127.0.0.1', 1440976653, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTc2NTk0Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('1bd7324ef78b5f8ba8e63fe378acbef59993d6cd', '127.0.0.1', 1440974337, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTc0MzA3Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('a63d6f2d83d244a65c6e76542216333de5739723', '127.0.0.1', 1440974936, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTc0ODg1Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('504471f6302dedee7d5285b28cd117dfe3ec9126', '127.0.0.1', 1440980267, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTgwMTU5Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('7a22abbdf7a073c80afd67908ce1729d189fab3e', '127.0.0.1', 1441020816, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxMDIwODEwOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('cec78c516075ec1328cf1ea18b1f4b069e317293', '127.0.0.1', 1440977000, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTc2OTk4Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('6e8c30f32a74f2ead9a4bbf06835e844b3c48cce', '127.0.0.1', 1440979399, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTc5MTIxOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('e783e191e89f95c697a6f5219d69ad88a771b13a', '127.0.0.1', 1440977626, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTc3NDUxOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('bd0be31554c20dce8eab7ac3ae33dd2bd7ff01a4', '127.0.0.1', 1440978260, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTc4MDQ3Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('2ed55b125beb487549fbe1476bf78ce78fa16543', '127.0.0.1', 1441495954, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNDk1NjYxOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('d897dc8ff6cdb608d52063a0def0c1d21920eaa3', '127.0.0.1', 1440979794, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTc5NTM0Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('661f9f3c8e1a4afe9d0d5017769e6088d5533a01', '127.0.0.1', 1440978518, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTc4MzU3Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('4d4f76b9b55220155551a15fe3df1f8ad0a81a0f', '127.0.0.1', 1441499113, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNDk4ODE5Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('a1827310c01714da8f6c5642fcb5947441c5cfa7', '127.0.0.1', 1441497682, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNDk3NTEyOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('cb67157acb78aed756ab79bdb9ca961eaf416f37', '127.0.0.1', 1440975450, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTc1MjA2Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('39fdf187c03e51616160904bb01b26cd0f717069', '127.0.0.1', 1441024583, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxMDI0MjkyOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('a22bb50e30e517e398113df0a5c38dcd500c2e9f', '127.0.0.1', 1441024635, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxMDI0NjM1Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('fcb612a06a0b93760f3e1ad845e1cf7dcc2615ec', '127.0.0.1', 1441498790, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNDk4NTA5Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('914eb1be312fa5588a791e95be18ff666b665396', '127.0.0.1', 1441498439, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNDk4MTcyOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('eb9817293937921d6a57a3a00393cca3e84a0076', '127.0.0.1', 1440975893, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTc1NjQ5Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('55842a323d8414a505293f44831476490389c01a', '127.0.0.1', 1441021873, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxMDIxNjI4Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('bca84352384edd5eaafa48078eaeedbd765c3e46', '127.0.0.1', 1440980143, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTc5ODQ0Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('024742a6ac1e840cdb4dd9385f2b22e2b3d1f1c2', '127.0.0.1', 1440976256, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTc2MjU1Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('f3d2b6bb6a6f94a101d39ceb36846a47bb0466db', '127.0.0.1', 1441024991, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxMDI0OTkwOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('79c940641092857f8b235dbb6f2b15ecc18f2835', '127.0.0.1', 1440979086, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQwOTc4ODA0Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('c76160853630fa458e48448adf3ff6f8a192fe49', '127.0.0.1', 1441031536, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxMDMxNTM1Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('4a53c551ef145b8a135b918249febe5064cae8b2', '127.0.0.1', 1441496129, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNDk2MDgzOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('74da84ba47f156225a35aaef0446852701dab847', '127.0.0.1', 1441022171, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxMDIxOTQxOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('c43235e85c33ddb22a7d8233932e80e846ab3e65', '127.0.0.1', 1441469233, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNDY5MTE5Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('af4e0766d87849a2b7864e72c4dfda533e5cbe8b', '127.0.0.1', 1441469575, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNDY5NTc1Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('01970d387b4d5ac7e9a6f1539a8fa4200a156d95', '127.0.0.1', 1441479185, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNDc5MTg1Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('31534cbc4aade96dea343b1b642c77ffc5deeb90', '127.0.0.1', 1441497306, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNDk3MDA2Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('69d9e4a7bbcbed695a3659f61f5f4ef6ff0cc008', '127.0.0.1', 1441496677, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNDk2MzkxOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('12f18e646f266fe827b0cba0a61c9b786a98d6bd', '127.0.0.1', 1441498138, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNDk3ODcxOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('c1d19295963bfaa95629710603dde7502e1b4965', '127.0.0.1', 1441496751, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNDk2NzAwOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('1c3d35113825f618f02caf62474dcae503f22317', '127.0.0.1', 1441507992, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNTA3ODQ2Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('cb48ca7a221bfd9f42bb4459139b6fd4cfd5f27b', '127.0.0.1', 1441502394, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNTAyMTI5Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('a5d0d2ff1550ed0efb6134a1c2752a9d3c8e70fa', '127.0.0.1', 1441499418, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNDk5MTUxOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('d289c05c7c69bff1ebe5e3f19ff0341df4a43999', '127.0.0.1', 1441553561, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNTUzNTYxOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('843f0b27fa380d3dd7dd610edbe64e763724be14', '127.0.0.1', 1441503289, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNTAzMTMyOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('b5406f6d5e9b28af8eab364db4a810d4204369c5', '127.0.0.1', 1441499863, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNDk5NTgxOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('c126ff31e5d8cebeab95ac2494efd104b8dc8d08', '127.0.0.1', 1441502816, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxNTAyNTQ1Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('d73a759d4608f47fb7707b431713faa3bbd88f56', '127.0.0.1', 1441832235, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxODMyMDY2Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('79183876699008edb624ce3b19b58b559b68e478', '127.0.0.1', 1441832507, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxODMyMzc2Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('06229532bc65e8d03161f141e099c3ed2dfefa3c', '127.0.0.1', 1441833575, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxODMzNTU4O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('839acb5470312ef83ccab59c0fa349c083b49ec9', '127.0.0.1', 1442003601, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDAzMzYxOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('293e193264eefb9acb5625af8090d180a23438be', '127.0.0.1', 1441837134, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxODM2ODQzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('27c4efb8028b96092e87e2019004acbedafb1faa', '127.0.0.1', 1442005677, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDA1NDU0O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('45e3f2e9b77276ae728ce496c97fa767f3c2c0c2', '127.0.0.1', 1441834501, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxODM0MzIyO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('f1f27c783d5ea63b5e23eff140e9330931e148c6', '127.0.0.1', 1441834934, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxODM0ODkxO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('3d9bf437083ca647428c2b9e8ce28ad7db73ec66', '127.0.0.1', 1441836744, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxODM2NTE1O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('2797acdc534b744dcfd49d62841bc775018bea5d', '127.0.0.1', 1441837448, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxODM3MTU2O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('195dd9831fc70ff815b5bb301529c860c79bf9ba', '127.0.0.1', 1441835731, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxODM1NDM2O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('7aa36f321445f2bc0baa6861a9401c9e1e5b539f', '127.0.0.1', 1441837468, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxODM3NDY4O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('0071b87e0a58c5ba91ec2e81049c00290f9f3f59', '127.0.0.1', 1441835946, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQxODM1NzQ1O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('3f22937b2d524958c06bc29b8a198fd0887c386a', '127.0.0.1', 1442001378, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDAxMzc3Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('4615cd6e55851a3a04fd1b12f18e301728525713', '127.0.0.1', 1442005387, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDA1MTIxO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('8be5ff43600520fb9194e10303319f7ed647bbf4', '127.0.0.1', 1442002008, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDAxNzY0Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('34ca2df1ffbb6619f870339d6ebedd89d1b13fae', '127.0.0.1', 1442004007, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDAzODAxO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('aa07b361b1ac23905dd813ea9246635987f3266d', '127.0.0.1', 1442005102, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDA0ODA0O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('860841fc6c8d1bb171a071f8e7d512dd47b52d60', '127.0.0.1', 1442002850, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDAyNTkwOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('a8221b9e5e188ca8573b005212f6a222f898f03c', '127.0.0.1', 1442006739, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDA2NDM5O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('5c06fda3c90d1fca75050bb61b9b50638940f3ff', '127.0.0.1', 1442006373, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDA2MDg1O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('94237dc5d3c310afb27dcc2edc43a479a923d82b', '127.0.0.1', 1442003306, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDAzMDU0Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('f5b6f9ce75532ae4239a430466a7344b41796a27', '127.0.0.1', 1442005949, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDA1NzU2O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('283a24c9ee02cbb52fc10a87552c59b7978f7271', '127.0.0.1', 1442007012, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDA2NzQ5O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('b8171683636d45805fa228a48fd3de70f8aacee1', '127.0.0.1', 1442007270, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDA3MTY4O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('0165192543aa5a6284d020dc79de5373f10bc828', '127.0.0.1', 1442004638, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDA0NDA5Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('4dce9c26f51d210e4b9164eac8c20d1b0861b0d0', '127.0.0.1', 1442007773, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDA3NDc4O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('56a206e8f8c8eef192b9e4abd2f1eb12ef22c372', '127.0.0.1', 1442007980, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDA3ODAyO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('138af7ab861a87d488c9824906adfc5f2cf8764b', '127.0.0.1', 1442010566, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDEwNDk1O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('c3d8fbf501db9d6776bb9349c15799ce5d1651e0', '127.0.0.1', 1442008396, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDA4MTg0O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('c61166dbd3ae13d71790015c72a56ed9ddb25add', '127.0.0.1', 1442178948, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTc4ODQwO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjIiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('f6e58ccc71ecbb1d10dc77a0c6a921329230e59e', '127.0.0.1', 1442011330, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDExMDcwO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('7e6cb96fde895260805a6302119c2a0e8fb00662', '127.0.0.1', 1442012649, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDEyMzYyO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('556aa9c50e6b7510fe74a187ad3bf4f24ecb9d71', '127.0.0.1', 1442008686, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDA4NTIyO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('ec34194b398b278ab5de77300c10299867e649dc', '127.0.0.1', 1442010163, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDA5ODgzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('bb787b6697916a33fbb5f2d1670c9f85250cb071', '127.0.0.1', 1442011760, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDExNjIxO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('830b668e5d88b8786698a242bcb25d686ab9e7d4', '127.0.0.1', 1442010250, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDEwMTkxO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('09a4f9ba7b1794376257abf096c028078789a521', '127.0.0.1', 1442009197, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDA4OTE3O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('e554074851163637d6d494caa253499739c48b3c', '127.0.0.1', 1442009734, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDA5NDM1O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('80309d7e773cdb38b04465c0b03421f59b91a5aa', '127.0.0.1', 1442012810, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDEyODEwOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('a44edfedecb968f946a861b6d2c71f741bbbc793', '127.0.0.1', 1442159885, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTU5ODg0Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('250dd1bad947202a3be7b25d3d9cd366d11baf7f', '127.0.0.1', 1442012297, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMDEyMDQxO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('cb4fd0de202d068f0202c744bca145af3657084e', '127.0.0.1', 1442178987, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTc4OTY4O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjIiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('dbc61ecce64d29563793193981d153dd88fd3953', '192.168.0.11', 1442163467, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTYzNDAzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('473275aa6ce5f59b77f5d1176603e8ffe2b6c30a', '127.0.0.1', 1442170951, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTcwOTUxOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('9b12667237a4eacb87d8f80a185bbfce1b275a87', '127.0.0.1', 1442161489, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTYxNDg5Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('073cb14cb9894370ea97f65f49c2d1eb3520ef84', '127.0.0.1', 1442178746, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTc4NTgyO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjIiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('3e9c58247273bd9c0f3f8d1e196c07686939b7cc', '127.0.0.1', 1442178282, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTc4MTc4O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjIiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('c343d23f73ad8124c85a9584ffe9a24cd87d7496', '127.0.0.1', 1442180312, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTgwMzEwO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjIiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('a1d4c8dd32055e0192331ec242be2063380a61d2', '127.0.0.1', 1442180870, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTgwNzYxO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjIiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('c41ee1946b66709734301edfe4a7b4e966c7c247', '127.0.0.1', 1442180941, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTgwODc2O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('336ac701b8579f63808a474c34f1998fdb155e6c', '127.0.0.1', 1442182711, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTgyNDE3O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('69be5684f96cc1a42bd168bb0c3338c958693b27', '127.0.0.1', 1442182981, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTgyNzQyO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('77ad941c5f884fa6ba415a4361de1ba9dae90a76', '127.0.0.1', 1442233304, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMjMzMTU3O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('aec9874953aa7dd3c64c924ae1e297e5009cd23c', '127.0.0.1', 1442188273, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTg3OTczO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('bb96a3fae70b21acb75a5600cbd4f40bfab3a783', '127.0.0.1', 1442187480, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTg3MzI0O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('cd5878fe5f77658df6973e12663f69ccf2bff298', '127.0.0.1', 1442186971, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTg2Njk3O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('f8dfb09067cb9d870186dbbad93c5214f5a8b2b9', '127.0.0.1', 1442187747, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTg3NjI5O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('be10973d1f7d1f091e7aa52af220174572e54996', '127.0.0.1', 1442186195, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTg1ODk1O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('c6dbccd7aae2f459db0f44c4864b83418ded6e93', '127.0.0.1', 1442189876, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTg5Njc5O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('2f733f9f5321a9ff957ae1bee26a6bef5f8d1eb4', '127.0.0.1', 1442189618, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTg5MzMwO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('edf551c22f14c0f0054522403b2058d4f130d399', '127.0.0.1', 1442189319, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTg5MDIxO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('0e001a024cc4eacf32f6f8692e3ffd4cd553ecd2', '127.0.0.1', 1442186569, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTg2Mjc0O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('64ad9a025885f680a4768375584261a6077bc0d6', '127.0.0.1', 1442188776, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTg4NjMyO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('e0c868803bc46baf8cec80877745fe9d9eb20eca', '127.0.0.1', 1442187282, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTg3MDA2O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('a8328d5898361b69b51e3776d281cf57908b07cc', '127.0.0.1', 1442188454, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTg4MzEwO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('b1930fb897acb7faf70b91086840c2a1aa6b763b', '127.0.0.1', 1442235477, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMjM1MjYzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('166cb08a03622f7737d6413adbc520c1ce1ef955', '127.0.0.1', 1442196419, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTk2NDE5Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('b84444c0df19ee6bf0f0001d7f347bf9d1a95b3f', '127.0.0.1', 1442197036, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMTk3MDMwO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('eb69de9be67468fb3b7cd5c4a8dca1847a1e6f4c', '127.0.0.1', 1442232186, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMjMyMTg0Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('3f9def7d961bd3936cba24c2d8a6a12c1481d5ad', '127.0.0.1', 1442235021, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMjM0NzMxO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('e0eeeab51c01d4b12a71f4fc30d25a83dc2282c3', '127.0.0.1', 1442235814, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMjM1NTY2O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('b9f93cc8ef773afa70271e589d0f6a0c363adaf6', '127.0.0.1', 1442236389, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMjM2MjYzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('dc4403f705b791c5c3e256a91be73c39f594f4b3', '127.0.0.1', 1442357882, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMzU3NjI3O3JlZGlyX3BhcmF8czoyMzoiaHR0cDovL2xvY2FsaG9zdC9hdmlzb3MiO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('c74d961915063ef408db61e636ac903e522208c7', '127.0.0.1', 1442939681, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTM5MzkxOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('3304dbec7a1c67808dc2485878e4c6582205319d', '127.0.0.1', 1442940415, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTQwMTc2Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('8ffd4d34872bd6fe0d3e247e5b5684789d16dd06', '127.0.0.1', 1442940647, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTQwNjM3Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('24ffe1550783ac1b39daeec8462cfe494a3c0a66', '127.0.0.1', 1442766674, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyNzY2NjY4O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('9fcabcb376b0426e126d47126ad7cf580715e617', '127.0.0.1', 1442358918, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMzU4NzE2O3JlZGlyX3BhcmF8czoyMzoiaHR0cDovL2xvY2FsaG9zdC9hdmlzb3MiO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('299ce1966ac820581a072825c2ce3dd87e3ae972', '192.168.0.12', 1442767620, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyNzY3NjIwOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('b990603b3556f474481f30c019e1c6157ffc85d2', '127.0.0.1', 1442359248, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMzU5MTk4O3JlZGlyX3BhcmF8czoyMzoiaHR0cDovL2xvY2FsaG9zdC9hdmlzb3MiO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('d09606de8100ae7360454a0e46f26a3bcf7f4920', '127.0.0.1', 1442360206, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMzU5OTU3O3JlZGlyX3BhcmF8czoyMzoiaHR0cDovL2xvY2FsaG9zdC9hdmlzb3MiO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('f7fcce18bb1df62e475cfcaa49228d8a2933aa5b', '127.0.0.1', 1442360272, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyMzYwMjcyO3JlZGlyX3BhcmF8czoyMzoiaHR0cDovL2xvY2FsaG9zdC9hdmlzb3MiO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('d078c2a1ebe550a24e6c42ba21036db56172bbdd', '127.0.0.1', 1442661609, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyNjYxNjA4O3JlZGlyX3BhcmF8czoyMzoiaHR0cDovL2xvY2FsaG9zdC9hdmlzb3MiO2Vycm9sb2dpbnxzOjkwOiI8ZGl2IGNsYXNzPSJhbGVydC1ib3ggYWxlcnQiPjxwPkFjZXNzbyByZXN0cml0bywgZmHDp2EgbG9naW4gYW50ZXMgZGUgcHJvc3NlZ3VpcjwvcD48L2Rpdj4iO19fY2lfdmFyc3xhOjE6e3M6OToiZXJyb2xvZ2luIjtzOjM6Im9sZCI7fQ==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('df08165d1ad8f87f287325d1f66b11bee1cbcc9a', '192.168.0.11', 1442698911, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyNjk4OTExOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('dfe339cc32ef204e07878411b07b29e35cb9e33f', '127.0.0.1', 1442836404, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyODM2MTM3O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiO21zZ2Vycm98czoxMDQ6IjxkaXYgY2xhc3M9ImFsZXJ0LWJveCBhbGVydCI+PHA+U2V1IHVzdcOhcmlvIG7Do28gdGVtIHBlcm1pc3PDo28gcGFyYSBleGVjdXRhciBlc3RhIG9wZXJhw6fDo288L3A+PC9kaXY+IjtfX2NpX3ZhcnN8YToxOntzOjc6Im1zZ2Vycm8iO3M6Mzoib2xkIjt9');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('01c6682fb232dbb11dda3f14f5209e35b0d0ecea', '127.0.0.1', 1442835357, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyODM1MTA1O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiO21zZ2Vycm98czoxMDQ6IjxkaXYgY2xhc3M9ImFsZXJ0LWJveCBhbGVydCI+PHA+U2V1IHVzdcOhcmlvIG7Do28gdGVtIHBlcm1pc3PDo28gcGFyYSBleGVjdXRhciBlc3RhIG9wZXJhw6fDo288L3A+PC9kaXY+IjtfX2NpX3ZhcnN8YToxOntzOjc6Im1zZ2Vycm8iO3M6Mzoib2xkIjt9');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('70eafe52f0192f8508b14dfd6efcb5b7ac0704df', '127.0.0.1', 1442836099, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyODM1ODA3Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('f738cad75cfa33863767b15f28c09a275877b835', '127.0.0.1', 1442834907, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyODM0NzMyO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('944a59704547f668a3e1777e1ab9bcc3ec53c763', '127.0.0.1', 1442835740, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyODM1NDU4O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiO21zZ2Vycm98czoxMDQ6IjxkaXYgY2xhc3M9ImFsZXJ0LWJveCBhbGVydCI+PHA+U2V1IHVzdcOhcmlvIG7Do28gdGVtIHBlcm1pc3PDo28gcGFyYSBleGVjdXRhciBlc3RhIG9wZXJhw6fDo288L3A+PC9kaXY+IjtfX2NpX3ZhcnN8YToxOntzOjc6Im1zZ2Vycm8iO3M6Mzoib2xkIjt9');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('b203b1fd992bdda0914874d6da5bf4cd7a162372', '127.0.0.1', 1442836944, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyODM2NjY2O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('d8e0292bde3db8001a8b72d8f571920539008c66', '127.0.0.1', 1442838419, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyODM4MTQ3O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('6c662c194029d0302fccffa1e641db3bd4f953dc', '127.0.0.1', 1442838038, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyODM3NzgzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('6ca90c1637ee9f41f1de5428752fd89f0b6f9de9', '127.0.0.1', 1442837764, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyODM3NDY1O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('a422fd7c8df8060f376b95209c254da3d11a854f', '127.0.0.1', 1442838737, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyODM4NDUxO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('28f1c786a0a4e8c62799f3e6b382470c92d78990', '127.0.0.1', 1442839004, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyODM4Nzk4O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('5f3b1347593a9a6cb616f6c18ce3f2fe604c657f', '127.0.0.1', 1442923253, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTIzMjQzOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('b351a6c6f51633763509a32febad3c8439f6c7ec', '127.0.0.1', 1442937944, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTM3NzE3Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('58ec6d999242dcece39b32fe3be4cc6e6b46d67d', '127.0.0.1', 1442938119, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTM4MDEwOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('9e372a8c92fbe9682d2bbbec2c16fe83e8791926', '127.0.0.1', 1442939047, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTM4NzU5Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('a81dd5f8e21614c315ee3c3df2ed5d9c6b83e895', '127.0.0.1', 1442939365, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTM5MDc5Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('67a5ecccb56ebe90fa34d8590eb8621f87d73d1b', '127.0.0.1', 1442962177, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTYyMDc1O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('1639d2a3c5307bb65a6ad5655f262c0e891dd5ab', '127.0.0.1', 1442963270, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTYzMjcwO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('a0600d2631e5885d3b5d08a9596cf50ba30438c5', '127.0.0.1', 1443650172, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjQ5OTcwO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('bfa52cb6ece97b9a50c4adc2a7455f7818f4fe56', '127.0.0.1', 1442941289, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTQxMTM0Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('4413eb2bbdcb2e032a60367212fbbba6997954c1', '127.0.0.1', 1443649924, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjQ5NjY4O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('3484506e199a7d9f01dbd9d2b1ede043c1147505', '127.0.0.1', 1443649597, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjQ5MzQ5O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('e119a23d598e4b2fb552c5c89455deda4f6122fa', '127.0.0.1', 1442946844, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTQ2ODE4O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('41689f767a977e5b9827535f6fd0f4f24f6d5d70', '127.0.0.1', 1442945553, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTQ1NDkyO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('51111b26f9d7a72754f6bd52ad814c1efa9c11e1', '127.0.0.1', 1442948193, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTQ4MDg2O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('516082a186356e76c789a23f651f63dc84319f25', '127.0.0.1', 1442961635, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTYxNjMzOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('0e3700c70b62c8fe1e18d45a63d2b022fa3b35aa', '127.0.0.1', 1442946143, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTQ2MTM1Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('8ce758d965aed5c6ffc56d8630e9c78cb8f94d04', '127.0.0.1', 1442963844, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTYzNjA1O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('a2fb4103cfa5a55902d6b85c8d3a73bf1268ddc6', '127.0.0.1', 1442963911, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTYzOTEwO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('f7cd1579dec755186984f9db49882296f3070d64', '127.0.0.1', 1442965276, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTY0OTgzO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('ccfc1ea203836bc16a1187dceab0d630df11fcad', '127.0.0.1', 1442965335, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQyOTY1MzM1O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('277652b8cd8a8b107ea27a88e96e969a15364ce8', '127.0.0.1', 1443193125, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzMTkzMTIyOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('792c390915b315487d8baf4ef475c7c64bc1a5a6', '127.0.0.1', 1443646237, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjQ2MjIzO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('28caf9537c01d2eef2eca049121870c477e5c7f4', '192.168.0.12', 1443559579, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNTU5NTc5Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('9fca801b5ca8a7dff0ccf52cbcf8b6a285d70a9b', '127.0.0.1', 1443645610, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjQ1NjEwOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('26b5a3f843faf4ab16c839036642d95be2ce5a30', '127.0.0.1', 1443651067, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjUwODg3O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiO21zZ29rfHM6NzM6IjxkaXYgY2xhc3M9ImFsZXJ0LWJveCBzdWNjZXNzIj48cD5DYWRhc3RybyBlZmV0dWFkbyBjb20gc3VjZXNzbzwvcD48L2Rpdj4iO19fY2lfdmFyc3xhOjE6e3M6NToibXNnb2siO3M6Mzoib2xkIjt9');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('2e85b4c45f845749334745eb61c5f16b38606da6', '127.0.0.1', 1443650046, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjQ5OTQ3O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiO21zZ29rfHM6NzY6IjxkaXYgY2xhc3M9ImFsZXJ0LWJveCBzdWNjZXNzIj48cD5BbHRlcmHDp8OjbyBlZmV0dWFkYSBjb20gc3VjZXNzbzwvcD48L2Rpdj4iO19fY2lfdmFyc3xhOjE6e3M6NToibXNnb2siO3M6Mzoib2xkIjt9');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('07b811f494d3a5ac4afa134fe8298e92292f5a7b', '127.0.0.1', 1443651155, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjUwODg1O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('f552cc7391e885841e625114f88c7582d59dda45', '127.0.0.1', 1443651397, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjUxMjA0O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('03190af1e9bd402881d3bb7a9a5c4508e30a70a4', '127.0.0.1', 1443651580, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjUxMzA2O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('766f3926e9894a58a8010a41787fb3feefc1ab39', '127.0.0.1', 1443653750, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjUzNTg2O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('bbcc15acff714c91fb081cac3d9ae404161a2383', '127.0.0.1', 1443653470, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjUzMjYwO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('042ce430956a5bb47655063e5191e24d2b2a6c1b', '127.0.0.1', 1443651685, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjUxNjU2O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('e1945cfd631f8ba2ff5af6bd551edfa0a0b4a9c0', '127.0.0.1', 1443659355, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjU5MjgwO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiO21zZ29rfHM6NzM6IjxkaXYgY2xhc3M9ImFsZXJ0LWJveCBzdWNjZXNzIj48cD5DYWRhc3RybyBlZmV0dWFkbyBjb20gc3VjZXNzbzwvcD48L2Rpdj4iO19fY2lfdmFyc3xhOjE6e3M6NToibXNnb2siO3M6Mzoib2xkIjt9');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('1984de32d6293d9887da31e994178c1a52962ab6', '127.0.0.1', 1443655744, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjU1NzM0O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('bfdcffd7f3c3f31daffbfdc6daaff5a2e28c8f20', '127.0.0.1', 1443655774, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjU1NjQ1O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('1e160b521e2cd18857dd3c8dca30429b55d69d04', '127.0.0.1', 1443657068, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjU2ODc1O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('30045a541b57bc27ede86bd1239569ce91519f7d', '127.0.0.1', 1443656169, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjU2MTM0O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('619e3ce277e7a26184aa40648ef090107c305f8b', '127.0.0.1', 1443659860, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjU5NjcwO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('de501f8d70be345411a8c27b89b241bd977b0e1d', '127.0.0.1', 1443658757, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjU4NjMxO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('16b5c643532d5702a0e11cc7ec06054d611d0325', '127.0.0.1', 1443652609, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjUyMzM4O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('ad6ae92cf0892ba7ceb82c9802ef99e06528899e', '127.0.0.1', 1443656822, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjU2NTYyO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('d81e2899e87d037623cfce9249a7a4d1f1ec3b6b', '127.0.0.1', 1443657441, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjU3MjgyO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('4f279c5eac0dfb33f4e0c94081789de80f6a5acd', '127.0.0.1', 1443657588, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjU3NTg4O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('4c2b22751edb8641bc575e69fadfcae1775799c5', '127.0.0.1', 1443753600, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNzUzMzA3O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('0e8e572ed3c112c532d9994a875b5ecad6de8216', '127.0.0.1', 1443753863, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNzUzNjEyO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('b268e7e5994fe657fe527cfede81aa3ec4bd1320', '127.0.0.1', 1443659426, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjU5MjYwO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('b4becd4e5f69a460675bc6d6b94e6be54dbf8429', '127.0.0.1', 1443660051, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNjU5OTg0O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('698e22509337b71304ac32c65447e45e179ce292', '127.0.0.1', 1443840444, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzODQwNDQxOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('a83798fef4ed55bdbbaa570b2cf737dd4c487ab9', '127.0.0.1', 1443753640, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzNzUzMzM4O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('f63f9d000bbd07a1afbacd34b4f69f94f099cdfc', '127.0.0.1', 1443968951, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzOTY4OTUxOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('16e68161b7296c7ca79a3d14b04268bff72c9723', '127.0.0.1', 1443983231, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzOTgzMjMwOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('09cc12f642e6bd588fdb3b50997949c7d43e594e', '127.0.0.1', 1443984134, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzOTg0MTE4O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('9a6ab2daf208ff21b4874563b8f697ab3a96a286', '127.0.0.1', 1443984677, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzOTg0NjcyO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('7d1305b8ffbb9cc2ae9cfdfe39cfdfe0d85baff3', '127.0.0.1', 1443985925, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzOTg1OTI1O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('41da3dd0713039c99b55f16909c2c886e7eb5def', '127.0.0.1', 1444602785, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjAyNjA1O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('4f015858827dafa75473eeffa6829a67d17c9529', '127.0.0.1', 1444601583, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjAxNDAxO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('5897d6d09c527c7aaff42661e6a5c1bec6f0afc5', '127.0.0.1', 1444601960, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjAxNzU3O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('571b0ffcd61edd84d91f41c4ae2b7aa11e01384a', '127.0.0.1', 1444175700, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0MTc1NDAzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('33d6e36d26dd354794d6339c525338eb576830fd', '127.0.0.1', 1444175706, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0MTc1NzA2O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('2f05197f684853575f7e66ebaaaa3cd5b0bbadd6', '127.0.0.1', 1444594718, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NTk0Njk4O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('b6561b078ecf010ac10e349792ded06c639a4463', '127.0.0.1', 1444599533, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NTk5NTMzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('558e13b83cd0acef5ffc5f9ab29b649926844073', '127.0.0.1', 1444175361, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0MTc1MDc4O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('b8b431220d84858e69c3f5e0c43c74b8946aa115', '127.0.0.1', 1444600309, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjAwMDQyO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('1e2bffe9a9ea0129c7156550512b2cc5b1d7989d', '127.0.0.1', 1443986472, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQzOTg2MjYwO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('645d2f46af10a7ddf99ff8a0de7a682e43486b28', '127.0.0.1', 1444164606, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0MTY0NjA2Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('39cfc82796a470af15c466a30d7f02614f658add', '127.0.0.1', 1444176730, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0MTc2NTU5O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('d292415a3e575d373874acba961cad2ccebf6015', '127.0.0.1', 1444421733, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NDIxNzMzOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('be756f8fc3f90f94e7885764d095f984847b4256', '127.0.0.1', 1444587694, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NTg3Njk0Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('c149cc5af0aa951433afe6e9d21ce4d8a29ea335', '127.0.0.1', 1444602522, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjAyMjIzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('e1176adc05cafab0a4c2442e02c3f2323aa4d5d0', '127.0.0.1', 1444604484, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjA0MjAxO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('878c54dfd47e2fe592b01c162a91e39d1379fb6d', '127.0.0.1', 1444604185, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjAzODg4O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('b6fed003da5f1739e2bda028dbec9ce4976272fa', '127.0.0.1', 1444603225, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjAzMDMwO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('f9b70e7538cd544b528702657e00759e7352f9a7', '127.0.0.1', 1444605482, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjA1MjIxO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('d29eceb5ef9532a7bc6b41507c1e45edb48bbeaf', '127.0.0.1', 1444605686, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjA1Njc1O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('4f22708058dd3272a0c67d9089246f6f390298a7', '127.0.0.1', 1444606406, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjA2MTE5O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('6708a485c34baeb50dd51fc363ca89d2e4b57e4a', '127.0.0.1', 1444606727, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjA2NDI5O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('024943ef3247d24d59d19846ab8fbf4cc050b682', '127.0.0.1', 1444943229, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTQyOTQ3O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('adc7addfc2b09d3396c48303e2e9327e0354ea55', '127.0.0.1', 1444610343, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjEwMzI0O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('4beef8ff6912997b7f5928b1fbdc88d966c5504d', '127.0.0.1', 1444606855, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjA2Nzc2O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('27033f5f5558e60b05d82250abef8508f64f96ee', '127.0.0.1', 1444643009, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjQzMDA3Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('ee635d7e226bad0a9a4373405a33fa420b394870', '127.0.0.1', 1444943256, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTQzMjU2O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('94ff7a2805eed54cdf6c2829a78546b5a29cc8d1', '127.0.0.1', 1444607405, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjA3MTExO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('a49818288c28d24a768171b8f133256959fd43ec', '127.0.0.1', 1444942539, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTQyNTMzO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('29fd1925939b45562918ffe40d7d9fdd182ffdf9', '127.0.0.1', 1444607719, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjA3NDE5O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('927755ed69b1e4b52f25eedc32f5b15e07b486c8', '127.0.0.1', 1444607723, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjA3NzIzO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('ed576dee098e3b38189a06221320340922e71020', '127.0.0.1', 1444943757, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTQzNzE0O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('3634f947233fd5ab631635722277681ff1acd996', '127.0.0.1', 1444655822, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NjU1ODE3O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('43a5ec4228d145bd9c78b00867ab41d074b0fdb1', '127.0.0.1', 1444768365, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0NzY4MzY0Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('54412f7b191fbc33ce7fdb50aaf2d40acd73f4e1', '127.0.0.1', 1444856782, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0ODU2NzgyOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('835df2fcb58d3b14c4ea59f601510a53b20550c9', '127.0.0.1', 1444861526, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0ODYxNDk2O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('0c0a0ccfea9b6121ab99450de67ff580c2e9c2f2', '127.0.0.1', 1444934402, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTM0NDAxOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('11cea1ac37bb1ab8972c4a07d3dada2951c284c3', '127.0.0.1', 1444935712, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTM1NzEwOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('29f6882e4dd7cc4fc44b9a78435cc7180f4c3531', '127.0.0.1', 1444945800, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTQ1NTQ1O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('2584da9f08b99ea5a170c401b9bbd49ab2648016', '127.0.0.1', 1444944372, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTQ0MTAzO3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('3ca00848a9d6c363023b76bf55af6a962d918469', '127.0.0.1', 1444944827, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTQ0NTM3O3VzZXJfaWR8czoxOiI2Ijt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjAiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('e2321c333b6d002165dff03600237bbd016b97d3', '127.0.0.1', 1444945443, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTQ1MTg4O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('a4205eec23dcac601417775fed868a15d7de1153', '127.0.0.1', 1444946719, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTQ2NDM5O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('7368a9983675e83399db6250305742bf149d6ab0', '127.0.0.1', 1444946162, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTQ1OTQ3O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('6dc0f3a0a604fc2c18594607264576207218a12b', '127.0.0.1', 1444946919, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTQ2NzUwO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('e3648eaf84c5f6ff141369170e119bdb5780a36c', '127.0.0.1', 1444950362, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTUwMDczO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('e5f9048cf049a68ed74eaf047551b040ebcb8eca', '127.0.0.1', 1444954153, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTU0MTUzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('1769bedd233dfe0d89507cbcb34557e0a19292d1', '127.0.0.1', 1444955136, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTU0ODY4O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('c38049673aec1ed252203ae4e993291791bafa45', '127.0.0.1', 1444947870, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTQ3NjM1O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('23f0dcb8db86d2a8d141468ed0da5abeed72e0c2', '127.0.0.1', 1444947942, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTQ3OTQyO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('c19af064a89c237a43c91b3412eec2d05a662f6c', '127.0.0.1', 1444951953, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTUxNzEwO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('f22a87dcee9cc30b86c54a1a34f105314c5ac390', '127.0.0.1', 1444950893, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTUwNzMzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('1002de0af8d8529cfb853c88ea03990697d5765f', '127.0.0.1', 1444951320, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTUxMDg1O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('f2921bf56d3db2cc1be8a0f0bbb1939f707a25a3', '127.0.0.1', 1444952782, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTUyNDk1O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('92f4f8b3a9414adb98a3e2ef9fd176d2f618ad85', '127.0.0.1', 1444949960, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTQ5NzM5O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('750b2498b6ffe3e512af2b9bd295e9f590604db4', '127.0.0.1', 1444955551, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTU1Mjk0O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('070b6998b0c797a610b600af9461ce380a8e3c78', '127.0.0.1', 1444952846, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTUyODA2O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('4bdca451a0c9d3c41683513a1a51d8bbe6af9759', '127.0.0.1', 1444954153, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTU0MTUzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('1088ae33f9b1e44c6198fb7dae11c880d2b6df63', '127.0.0.1', 1444951463, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTUxNDA1O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('2ee6e5e33d935a985263baf43463cc9117aa84db', '127.0.0.1', 1444954153, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTU0MTUzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('0287969f070390e1efa50f737b84cc14daaf8206', '127.0.0.1', 1444952377, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTUyMTM2O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('bc448ee8e2105eaf4cf4dc776ce0eaff0d422f01', '127.0.0.1', 1444954728, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTU0NTYxO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('2ea85e7459d13d7c3a2b410cc5e4967820e1a83d', '127.0.0.1', 1444954504, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTU0MTUzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('b166fd5dc08dac73b33d3fe6d40f5b6eb8b2dfcb', '127.0.0.1', 1444956233, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTU2MDMzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('6f47d2cd583e9c99f482177697d6425d20b8b8a3', '127.0.0.1', 1444956683, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTU2Mzg3O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('52fff2c85c59c6c4dea8e2984b4e848fdbbfbff5', '127.0.0.1', 1444963568, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTYzNTY3O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('0abd2be0d86dc2d68d222856875f2cc99f12c536', '127.0.0.1', 1444957089, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTU2NzgzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('c4515e3011ea1472734dd0e9b14a16a9643f330a', '127.0.0.1', 1445030470, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ1MDMwMzUzO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('e19e21fdb2e32dcbd7d10f9aeaea82eb6d8f8cd4', '127.0.0.1', 1445029208, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ1MDI5MDU4O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('e570c705563de6c2cad3fd1a56f44eade4be5540', '127.0.0.1', 1445028275, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ1MDI4MDQxO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('cfd954222a9b55aa377b25e17584a03a39e6f17a', '127.0.0.1', 1445027344, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ1MDI3MzI4O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('91e0982469b000358b8213b33d09a65337914f8d', '127.0.0.1', 1445027932, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ1MDI3NjU3O3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('3c169cb38afd9fcfadefb0f5510d02e08594e1e7', '127.0.0.1', 1444963667, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ0OTYzNjY3Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('a1bc78eac60759205354856819b2b883aaf5a00c', '127.0.0.1', 1445025121, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ1MDI1MTE5Ow==');
INSERT INTO ci_sessions (id, ip_address, "timestamp", data) VALUES ('82dcb02ac03d97527243f3b9b0cf1c8a020d8c26', '127.0.0.1', 1445029643, 'X19jaV9sYXN0X3JlZ2VuZXJhdGV8aToxNDQ1MDI5NDAwO3VzZXJfaWR8czoxOiIxIjt1c2VyX25vbWV8czoyOToiTUFSQ1VTIFZJTklDSVVTIEJSVU0gREEgQ09TVEEiO3VzZXJfaWRfY29uZG9taW5pb3xzOjE6IjEiO3VzZXJfY29uZG9taW5pb3xzOjg6IktPTkRPVEVLIjt1c2VyX2xvZ2Fkb3xiOjE7dXNlcl9uaXZlbHxzOjE6IjEiOw==');


--
-- TOC entry 2089 (class 0 OID 16412)
-- Dependencies: 172
-- Data for Name: condominio; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO condominio (id, nome, telefone, email, endereco, ativo, data_cadastro) VALUES (1, 'KONDOTEK', '55555555', 'contato@kondotek.com.br', 'RUA TESTE', 1, '2015-09-09 18:16:48.119188');


--
-- TOC entry 2125 (class 0 OID 0)
-- Dependencies: 171
-- Name: condominio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('condominio_id_seq', 1, true);


--
-- TOC entry 2103 (class 0 OID 16646)
-- Dependencies: 186
-- Data for Name: mensagens; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO mensagens (id, titulo, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (1, 'TITULO_TESTE', 'TEXTO_TESTE', 1, 1, '2015-10-15 20:14:05.898229', true);


--
-- TOC entry 2126 (class 0 OID 0)
-- Dependencies: 185
-- Name: mensagens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('mensagens_id_seq', 1, true);


--
-- TOC entry 2099 (class 0 OID 16586)
-- Dependencies: 182
-- Data for Name: ocorrencias; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO ocorrencias (id, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (1, 'TESTE DE OCORRÊNCIA2', 1, 1, '2015-10-04 16:19:45.421701', true);
INSERT INTO ocorrencias (id, texto, id_usuario, id_condominio, data_cadastro, ativo) VALUES (2, 'SDASDASD', 1, 1, '2015-10-04 16:20:09.1292', false);


--
-- TOC entry 2127 (class 0 OID 0)
-- Dependencies: 181
-- Name: ocorrencias_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ocorrencias_id_seq', 2, true);


--
-- TOC entry 2101 (class 0 OID 16619)
-- Dependencies: 184
-- Data for Name: ocorrencias_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO ocorrencias_usuario (id, visualizado, id_usuario, id_ocorrencia, id_condominio, data_cadastro) VALUES (1, false, 1, 1, 1, '2015-10-04 16:19:45.421701');
INSERT INTO ocorrencias_usuario (id, visualizado, id_usuario, id_ocorrencia, id_condominio, data_cadastro) VALUES (2, false, 1, 2, 1, '2015-10-04 16:20:09.1292');


--
-- TOC entry 2128 (class 0 OID 0)
-- Dependencies: 183
-- Name: ocorrencias_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ocorrencias_usuario_id_seq', 2, true);


--
-- TOC entry 2091 (class 0 OID 16422)
-- Dependencies: 174
-- Data for Name: usuarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO usuarios (id, nome, cpf, telefone, endereco, email, nivel_acesso, senha, id_condominio, ativo, data_cadastro, cidade, estado, pais) VALUES (1, 'MARCUS VINICIUS BRUM DA COSTA', '999999999', '5184962699', 'RUA TESTE 555 PORTO ALEGRE', 'marcusviniciusbrumcosta@gmail.com', 1, '$2y$10$411ldM.R1efgFdTUZ62yTugHrDc1ob/fchYaD6CQQKPWSgAi/0MWe', 1, 1, '2015-09-09 18:18:18.430989', NULL, NULL, NULL);
INSERT INTO usuarios (id, nome, cpf, telefone, endereco, email, nivel_acesso, senha, id_condominio, ativo, data_cadastro, cidade, estado, pais) VALUES (6, 'MARCUS VINICIUS BRUM DA COSTA', '999999999', '5184962699', 'RUA TESTE', 'marcusbc@objetus.com.br', 0, '$2y$10$411ldM.R1efgFdTUZ62yTugHrDc1ob/fchYaD6CQQKPWSgAi/0MWe', 1, 1, '2015-09-13 18:01:23.723814', 'PORTO ALEGRE', 'RS', 'BRASIL');


--
-- TOC entry 2129 (class 0 OID 0)
-- Dependencies: 173
-- Name: usuarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('usuarios_id_seq', 6, true);


--
-- TOC entry 2095 (class 0 OID 16494)
-- Dependencies: 178
-- Data for Name: votacao; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO votacao (id, titulo, id_usuario, id_condominio, data_cadastro, ativo, finalizada) VALUES (1, 'VOTAÇÃO TESTE 1', 1, 1, '2015-09-22 20:17:20.254861', true, false);
INSERT INTO votacao (id, titulo, id_usuario, id_condominio, data_cadastro, ativo, finalizada) VALUES (3, 'REFORMA DA FACHADA', 1, 1, '2015-10-01 23:38:57.449068', true, false);
INSERT INTO votacao (id, titulo, id_usuario, id_condominio, data_cadastro, ativo, finalizada) VALUES (2, 'JDFHKFJHSDJFSD', 1, 1, '2015-09-30 20:29:02.720557', false, false);


--
-- TOC entry 2130 (class 0 OID 0)
-- Dependencies: 177
-- Name: votacao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('votacao_id_seq', 3, true);


--
-- TOC entry 1954 (class 2606 OID 16475)
-- Name: pk_avisos; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY avisos
    ADD CONSTRAINT pk_avisos PRIMARY KEY (id);


--
-- TOC entry 1958 (class 2606 OID 16559)
-- Name: pk_avisos_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY avisos_usuario
    ADD CONSTRAINT pk_avisos_usuario PRIMARY KEY (id);


--
-- TOC entry 1946 (class 2606 OID 16408)
-- Name: pk_ci_sessions; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ci_sessions
    ADD CONSTRAINT pk_ci_sessions PRIMARY KEY (id);


--
-- TOC entry 1948 (class 2606 OID 16419)
-- Name: pk_condominio; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY condominio
    ADD CONSTRAINT pk_condominio PRIMARY KEY (id);


--
-- TOC entry 1964 (class 2606 OID 16656)
-- Name: pk_mensagens; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mensagens
    ADD CONSTRAINT pk_mensagens PRIMARY KEY (id);


--
-- TOC entry 1960 (class 2606 OID 16596)
-- Name: pk_ocorrencias; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ocorrencias
    ADD CONSTRAINT pk_ocorrencias PRIMARY KEY (id);


--
-- TOC entry 1962 (class 2606 OID 16626)
-- Name: pk_ocorrencias_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ocorrencias_usuario
    ADD CONSTRAINT pk_ocorrencias_usuario PRIMARY KEY (id);


--
-- TOC entry 1950 (class 2606 OID 16433)
-- Name: pk_usuarios; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuarios
    ADD CONSTRAINT pk_usuarios PRIMARY KEY (id);


--
-- TOC entry 1956 (class 2606 OID 16505)
-- Name: pk_votacao; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY votacao
    ADD CONSTRAINT pk_votacao PRIMARY KEY (id);


--
-- TOC entry 1952 (class 2606 OID 16435)
-- Name: unk_email; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuarios
    ADD CONSTRAINT unk_email UNIQUE (email);


--
-- TOC entry 1967 (class 2606 OID 16481)
-- Name: fk_avisos_condominios; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avisos
    ADD CONSTRAINT fk_avisos_condominios FOREIGN KEY (id_condominio) REFERENCES condominio(id);


--
-- TOC entry 1972 (class 2606 OID 16570)
-- Name: fk_avisos_usuario_avisos; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avisos_usuario
    ADD CONSTRAINT fk_avisos_usuario_avisos FOREIGN KEY (id_aviso) REFERENCES avisos(id);


--
-- TOC entry 1970 (class 2606 OID 16560)
-- Name: fk_avisos_usuario_condominio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avisos_usuario
    ADD CONSTRAINT fk_avisos_usuario_condominio FOREIGN KEY (id_condominio) REFERENCES condominio(id);


--
-- TOC entry 1971 (class 2606 OID 16565)
-- Name: fk_avisos_usuario_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avisos_usuario
    ADD CONSTRAINT fk_avisos_usuario_usuario FOREIGN KEY (id_usuario) REFERENCES usuarios(id);


--
-- TOC entry 1966 (class 2606 OID 16476)
-- Name: fk_avisos_usuarios; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY avisos
    ADD CONSTRAINT fk_avisos_usuarios FOREIGN KEY (id_usuario) REFERENCES usuarios(id);


--
-- TOC entry 1965 (class 2606 OID 16436)
-- Name: fk_condominio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuarios
    ADD CONSTRAINT fk_condominio FOREIGN KEY (id_condominio) REFERENCES condominio(id);


--
-- TOC entry 1978 (class 2606 OID 16657)
-- Name: fk_mensagens_condominios; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY mensagens
    ADD CONSTRAINT fk_mensagens_condominios FOREIGN KEY (id_condominio) REFERENCES condominio(id);


--
-- TOC entry 1979 (class 2606 OID 16662)
-- Name: fk_mensagens_usuarios; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY mensagens
    ADD CONSTRAINT fk_mensagens_usuarios FOREIGN KEY (id_usuario) REFERENCES usuarios(id);


--
-- TOC entry 1973 (class 2606 OID 16597)
-- Name: fk_ocorrencias_condominios; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ocorrencias
    ADD CONSTRAINT fk_ocorrencias_condominios FOREIGN KEY (id_condominio) REFERENCES condominio(id);


--
-- TOC entry 1976 (class 2606 OID 16632)
-- Name: fk_ocorrencias_usuario_condominio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ocorrencias_usuario
    ADD CONSTRAINT fk_ocorrencias_usuario_condominio FOREIGN KEY (id_condominio) REFERENCES condominio(id);


--
-- TOC entry 1975 (class 2606 OID 16627)
-- Name: fk_ocorrencias_usuario_ocorrencias; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ocorrencias_usuario
    ADD CONSTRAINT fk_ocorrencias_usuario_ocorrencias FOREIGN KEY (id_ocorrencia) REFERENCES ocorrencias(id);


--
-- TOC entry 1977 (class 2606 OID 16637)
-- Name: fk_ocorrencias_usuario_usuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ocorrencias_usuario
    ADD CONSTRAINT fk_ocorrencias_usuario_usuario FOREIGN KEY (id_usuario) REFERENCES usuarios(id);


--
-- TOC entry 1974 (class 2606 OID 16602)
-- Name: fk_ocorrencias_usuarios; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ocorrencias
    ADD CONSTRAINT fk_ocorrencias_usuarios FOREIGN KEY (id_usuario) REFERENCES usuarios(id);


--
-- TOC entry 1969 (class 2606 OID 16511)
-- Name: fk_votacao_condominio; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY votacao
    ADD CONSTRAINT fk_votacao_condominio FOREIGN KEY (id_condominio) REFERENCES condominio(id);


--
-- TOC entry 1968 (class 2606 OID 16506)
-- Name: fk_votacao_usuarios; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY votacao
    ADD CONSTRAINT fk_votacao_usuarios FOREIGN KEY (id_usuario) REFERENCES usuarios(id);


--
-- TOC entry 2110 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-10-16 18:25:55 BRT

--
-- PostgreSQL database dump complete
--

